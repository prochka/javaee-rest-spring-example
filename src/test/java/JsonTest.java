import com.codingcrayons.aspectfaces.plugins.j2ee.rest.entity.ExpandableHashMap;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.util.Map;

/**
 * @author Kamil Prochazka (Kamil.Prochazka@airbank.cz)
 */
public class JsonTest {

    @org.junit.Test
    public void test() throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        om.enable(SerializationFeature.INDENT_OUTPUT);
        om.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
        om.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

        Map<String, Object> map = new ExpandableHashMap();

        System.out.println(om.writeValueAsString(map));

        System.out.println(map.getClass().getName());

    }

    {
//        AFEntity entity = new AFEntity();
//        AFField field = new AFField(entity);
//        entity.addField(field);
////        entity.addProperty("testProperty", "ferda");
//        entity.addValidationMessage(new AFValidationMessage("code", "username", null, "Could not be empty string!"));
//        entity.setId(1L);
//        entity.setName("user");
    }

}
