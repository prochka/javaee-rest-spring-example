;
(function (global, AF) {

    if (!AF) {
        throw Error("AF namespace not defined!");
    }

    /**
     * AF REST AFForm mixin for defining HTML inputs.
     */
    AF.AFInputMixin = {

        getInitialState: function () {
            return {
                // Internal field converted value
                _value: this.props.value,
                // Is field required ?
                _isRequired: false,
                // Is field value valid ?
                _isValid: true,
                // Is field value unchanged since initialValue >
                _isFresh: true,
                // Initial field value, stored for reset
                _initialValue: this.props.value,
                _validationError: '',
                _externalError: null
            };
        },

        getDefaultProps: function () {
            return {
                validations: {},
                validationError: '',
                // validation errors translations
                validationErrors: {}
            };
        },

        componentWillMount: function () {

            if (!this.props.name) {
                throw new Error('AFInput requires a name property when used');
            }

            // closure for deferred invocation on this AF input
            var configure = function () {
                this.setValidations(this.props.validations, this.props.required);
                this.props._attachToForm(this);
            }.bind(this);

            // sometimes _attachToAFModel handler is not present during componentWillMount
            // defer to another event cycle
            if (!this.props._attachToForm) {
                return setTimeout(function () {
                    if (!this.isMounted()) {
                        return;
                    }
                    if (!this.props._attachToForm) {
                        throw new Error('AFInput Mixin requires component to be nested in a Form');
                    }
                    configure();
                }.bind(this), 0);
            } else {
                configure();
            }

        },

        // We have to make the validate method is kept when new props are added
        componentWillReceiveProps: function (nextProps) {
            this.setValidations(nextProps.validations, nextProps.required);
        },

        componentDidUpdate: function (prevProps, prevState) {
            // If validations has changed or something outside changes the value, set the value again running a validation
            if (this.props.value !== prevProps.value && this.state._value === prevProps.value) {
                this.setValue(this.props.value);
            }
        },

        // Detach it when component unmounts
        componentWillUnmount: function () {
            this.props._detachFromForm(this);
        },

        // Add validations to the store itself as the props object can not be modified
        setValidations: function (validations, required) {

            if (validations && typeof validations !== "object") {
                throw new Error("Prop 'validations' must be of object type!");
            }
            this._validations = validations || {};

            // required validations can be default or passed by object
            if (!required) {
                required = {};
            }
            this._requiredValidations = required === true ? {isDefaultRequiredValue: true} : required;
        },

        // We validate after the value has been set
        setValue: function (value) {
            this.setState({
                _value: value,
                _isFresh: false
            }, function () {
                this.props._validate(this);
            }.bind(this));
        },

        resetValue: function () {
            this.setState({
                _value: this.state._initialValue,
                _isFresh: true
            }, function () {
                this.props._validate(this);
            });
        },

        getValue: function () {
            return this.state._value;
        },
        getErrorMessage: function () {
            return !this.isValid() || this.showRequired() ? (this.state._externalError || this.state._validationError) : null;
        },
        isFormDisabled: function () {
            return this.props._isFormDisabled();
        },
        isValid: function () {
            return this.state._isValid;
        },
        isFresh: function () {
            return this.state._isFresh;
        },
        isRequired: function () {
            return !!this.props.required;
        },
        showRequired: function () {
            return this.state._isRequired;
        },
        showError: function () {
            //return !this.showRequired() && !this.isValid();
            return !this.isValid();
        },
        isValidValue: function (value) {
            return this.props._isValidValue.call(null, this, value);
        }
    };
}).apply(window, [window, AF]);
