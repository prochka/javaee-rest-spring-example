var AF = AF || {};

(function (global, document, AF) {

    // START Util methods
    AF.Util = {
        arraysDiffer: function (arrayA, arrayB) {
            var isDifferent = false;
            if (arrayA.length !== arrayB.length) {
                isDifferent = true;
            } else {
                arrayA.forEach(function (item, index) {
                    if (item !== arrayB[index]) {
                        isDifferent = true;
                    }
                });
            }
            return isDifferent;
        },

        capitalizeFirstLetter: function (input) {
            if (input && typeof input === 'string') {
                return input.charAt(0).toUpperCase() + input.slice(1);
            }
            return input;
        },

        /**
         * Utility for building Metamodel full field name which is composed of
         * optional field prefix and required field name.
         *
         * @param {string} name the field name property
         * @param {string|void} prefix the optional field prefix property
         * @returns {string} the full fieldName value = <strong>[prefix.]name</strong>
         */
        buildFieldName: function (name, prefix) {
            // field name is constructed from 'prefix.name'
            return prefix ? prefix + "." + name : name;
        },

        /**
         * Format message with given arguments placeholders.
         *
         * Ex: formatString("Hello {0} {1}", "cruel", "world") = "Hello cruel world"
         *
         * @param {string} message string message to format
         * @param {void|*} list of arguments
         * @returns {string} formatted message
         */
        formatString: function (message) {
            if (typeof message !== 'string') {
                throw Error("Not a string");
            }

            var args = arguments;
            if (arguments.length == 2 && arguments[1] && arguments[1].constructor === Array) {
                args = [message].concat(arguments[1]);
            }
            return message.replace(/({)(\d+)(})/gi, function (match, group1, group2) {
                match = 1 * group2 + 1;
                if (args.length < match + 1) {
                    throw Error("Cannot format string: " + message);
                }
                return args[match]
            })
        },

        /**
         * Converts server returned validationMessages
         */
        convertValidationMessages: function (data) {
            if (typeof data !== "object" || data === null || !data.validationMessages) {
                return {};
            }

            var errors = data.validationMessages.reduce(function (previous, current) {
                var fieldName = current.path;
                var msg = current.message;

                if (previous[fieldName]) {
                    previous[fieldName].push(msg);
                } else {
                    previous[fieldName] = [msg];
                }

                return previous;
            }, {})

            return errors;
        },

        /**
         * Triggers native onChange event on given DOM element.
         * @param element the dom element
         */
        triggerOnChangeEvent: function (element) {
            if ("createEvent" in document) {
                var evt = document.createEvent("HTMLEvents");
                evt.initEvent("change", true, true);
                element.dispatchEvent(evt);
            }
            else {
                element.fireEvent("onchange");
            }
        }
    };
    // END Util methods

    // START ValueTransformers
    //  - transforms from/to HTML input value
    /**
     * ValueTransformer is responsible for converting HTML input values from domain model values to
     * HTML input string value.
     *
     * @param fromInput function transforming string value from HTML input
     * @param toInput function transforming value to HTML input value
     * @constructor construct new ValueTransformer with given fromInput, toInput converter or use default
     */
    var ValueTransformer = function (fromInput, toInput) {
        this._from = (typeof fromInput === 'function') ?
                     fromInput
            : function (value) {
            return value && value.toString ? value.toString() : value;
        };
        this._to = (typeof toInput === 'function') ?
                   toInput
            : function (value) {
            return value && value.toString ? value.toString() : value;
        };
    };
    ValueTransformer.prototype.fromInput = function (value) {
        return this._from.call(null, value);
    };
    ValueTransformer.prototype.toInput = function (value) {
        return this._to.call(null, value);
    };

    var DefaultValueTransformer = new ValueTransformer();
    var IdentityFunction = function (value) {
        return value;
    }
    var NoOpValueTransformer = new ValueTransformer(IdentityFunction, IdentityFunction);

    var ArrayTransformer = function (valueTransformer) {
        this._internalValueTransformer = valueTransformer;
    };
    ArrayTransformer.prototype.fromInput = function (value) {
        if (value && value.constructor === Array) {
            var that = this;
            var arr = [];
            value.forEach(function (item) {
                var s = that._internalValueTransformer.fromInput(item);
                if (s !== null) {
                    arr.push(s);
                }
            });
            return arr;
        }
        return this._internalValueTransformer.fromInput(value);
    };
    ArrayTransformer.prototype.toInput = function (value) {
        if (value && value.constructor === Array) {
            var that = this;
            var arr = [];
            value.forEach(function (item) {
                var s = that._internalValueTransformer.toInput(item);
                if (s !== null) {
                    arr.push(s);
                }
            });
            return arr;
        }
        return this._internalValueTransformer.toInput(value);
    };

    var padNumber = function (number) {
        number = number.toString();
        return number.length === 1 ? "0" + number : number;
    };

    var DateTimeValueTransformer = new ValueTransformer(function (value) {
        return value ? (new Date(value)).getTime() : '';
    }, function (value) {
        if (value) {
            var d = new Date(value);
            return d.getFullYear() + "-" + padNumber(d.getMonth() + 1) + "-" + padNumber(d.getDate()) + "T" + padNumber(d.getHours()) + ":"
                   + padNumber(d.getMinutes());
        } else {
            return "";
        }
    });

    var DateValueTransformer = new ValueTransformer(function (value) {
        return value ? (new Date(value)).getTime() : '';
    }, function (value) {
        if (value) {
            var d = new Date(value);
            return d.getFullYear() + "-" + padNumber(d.getMonth() + 1) + "-" + padNumber(d.getDate());
        } else {
            return "";
        }
    });

    var NumberValueTransformer = new ValueTransformer(function (value) {
        if (value) {
            var parsed = parseInt(value);
            if (parsed !== NaN) {
                return parsed;
            } else {
                return null;
            }
        }
        return value;
    });

    var ArrayNumberValueTransformer = new ArrayTransformer(NumberValueTransformer);

    AF.valueTransformers = {
        default: DefaultValueTransformer,
        noOp: NoOpValueTransformer,
        number: NumberValueTransformer,
        arrayNumber: ArrayNumberValueTransformer,
        date: DateValueTransformer,
        dateTime: DateTimeValueTransformer
    };

    AF.ValueTransformer = ValueTransformer;
    // END ValueTransformer

    AF.buildInputs = function (metamodel, data, options) {
        if (!metamodel || !metamodel.fields || !metamodel.fields.constructor === Array) {
            throw new Error("Metamodel object does not contains array of fields!");
        }

        var cfg = options || {};
        if (!cfg.fieldCallback || typeof cfg.fieldCallback !== 'function') {
            cfg.fieldCallback = function () {
            };
        }

        var inputs = [];

        metamodel.fields.forEach(function (field) {

            if (typeof field !== 'object' || field === null) {
                throw new Error("Metamodel field not an object!");
            }

            if (!field.name || !field.name) {
                throw new Error("Metamodel field has no 'name' property!");
            }

            var fieldName = AF.Util.buildFieldName(field.name, field.prefix);

            // Check if the field is not ignored
            if (cfg.ignore && cfg.ignore.constructor === Array && cfg.ignore.indexOf(fieldName) >= 0) {
                return;
            }

            var fieldInput = AF.createInput(metamodel, field, data || {}, cfg);
            if (fieldInput) {
                inputs.push(fieldInput);
            }
        });

        return inputs;
    };

    // START ValidationRules
    /**
     * AF REST validations rules and its translations
     */
    AF.validationRules = {
        'isDefaultRequiredValue': function (values, value) {
            return value === undefined || value === null || value === '';
        },
        'hasValue': function (values, value) {
            return value !== undefined;
        },
        'matchRegexp': function (values, value, regexp) {
            return value !== undefined && !!value.match(regexp);
        },
        'isUndefined': function (values, value) {
            return value === undefined;
        },
        'isEmptyString': function (values, value) {
            return value === '';
        },
        'isEmail': function (values, value) {
            return !value
                   || value.match(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i);
        },
        'isTrue': function (values, value) {
            return value === true || value === "true";
        },
        'isFalse': function (values, value) {
            return value === false || value === "false";
        },
        'isNumeric': function (values, value) {
            if (typeof value === 'number') {
                return true;
            } else {
                var matchResults = value !== undefined && value.match(/[-+]?(\d*[.])?\d+/);
                if (!!matchResults) {
                    return matchResults[0] == value;
                } else {
                    return false;
                }
            }
        },
        'isAlpha': function (values, value) {
            return !value || value.match(/^[a-zA-Z]+$/);
        },
        'isWords': function (values, value) {
            return !value || value.match(/^[a-zA-Z\s]+$/);
        },
        'isSpecialWords': function (values, value) {
            return !value || value.match(/^[a-zA-Z\s\u00C0-\u017F]+$/);
        },
        isLength: function (values, value, length) {
            return value !== undefined && value.length === length;
        },
        equals: function (values, value, eql) {
            return value == eql;
        },
        equalsField: function (values, value, field) {
            return value == values[field];
        },
        maxLength: function (values, value, length) {
            return value !== undefined && value.length <= length;
        },
        minLength: function (values, value, length) {
            return value !== undefined && value.length >= length;
        },
        minValue: function (values, value, min) {
            return value !== undefined && value >= min;
        },
        maxValue: function (values, value, max) {
            return value !== undefined && value <= max;
        }
    };

    /**
     * Translations for ValidationRules. Client should override or use these.
     */
    AF.validationRules.translations = {
        isDefaultRequiredValue: "Povinné pole",
        required: "Povinné pole",
        isTrue: "Povinné pole",
        isFalse: "Povinné pole",
        minLength: "Minimální délka musí být {0}",
        maxLength: "Maximální délka musí být {0}",
        minValue: "Minimální hodnota musí být {0}",
        maxValue: "Maximální hodnota musí být {0} ",
        isEmail: "Není validní emailová adresa",
        isNumeric: "Musí být číslo"
    };
    // END ValidationRules

}).apply(window, [window, document, AF]);
