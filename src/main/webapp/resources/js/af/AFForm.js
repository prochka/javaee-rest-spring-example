(function (global, AF) {

    if (!AF) {
        throw Error("AF namespace not defined!");
    }

    /**
     * AF REST Form component which server as container for HTML inputs, its converted values and validator.
     */
    AF.AFForm = React.createClass({

        getInitialState: function () {
            return {
                // We add a new state here, isValid, which will be true initially.
                // When inputs are attached they will be validated, in turn
                // changing this value to false if any inputs are invalid
                isValid: true,
                // Signaling that the submit button was pressed and should be
                // disabled till response is received
                isSubmitting: false,
                // Flag disabling calling onChange handler if Model is not fully processed
                canChange: false
            };
        },

        getDefaultProps: function () {
            return {
                onSuccess: function () {
                },
                onError: function () {
                },
                onSubmit: function () {
                },
                onValidSubmit: function () {
                },
                onInvalidSubmit: function () {
                },
                onSubmitted: function () {
                },
                onValid: function () {
                },
                onInvalid: function () {
                },
                onChange: function () {
                },
                validationErrors: null
                // TODO validationError ?
            };
        },

        // Add a map to store the inputs of the form, a model to store
        // the values of the form and register child inputs
        componentWillMount: function () {
            this.inputs = {};
            this.model = {};
        },

        // When the form loads we validate initial values if the delay validation is not active
        componentDidMount: function () {
            this.validateForm();
        },

        componentWillUpdate: function () {
            var inputKeys = Object.keys(this.inputs);

            // The updated children array is not available here for some reason,
            // we need to wait for next event loop
            setTimeout(function () {
                // The component might have been unmounted on an update
                if (this.isMounted()) {

                    if (this.props.validationErrors) {
                        this.setInputValidationErrors(this.props.validationErrors);
                    }

                    // Validate form in case new inputs were added
                    var newInputKeys = Object.keys(this.inputs);
                    if (AF.Util.arraysDiffer(inputKeys, newInputKeys)) {
                        this.validateForm();
                    }
                }

            }.bind(this), 0);
        },

        // Update model, submit to url prop and send the model to callback handlers
        submit: function (event) {
            // prevent default browser action
            event && event.preventDefault();

            // Trigger form as not fresh.
            // If any inputs have not been touched yet this will make them dirty
            // so validation becomes visible (if based on isFresh)
            this.setFormFresh(false);
            this.updateModel();
            var model = this.mapModelKeys();

            var callback = function () {
                this.props.onSubmit(model, this.resetModel, this.updateInputsWithError);
                if (this.state.isValid) {
                    this.props.onValidSubmit(model, this.resetModel, this.updateInputsWithError)
                } else {
                    this.props.onInvalidSubmit(model, this.resetModel, this.updateInputsWithError);
                }
            }.bind(this);

            setTimeout(callback, 0);
        },

        mapModelKeys: function () {
            return this.props.mapping ? this.props.mapping(this.model) : this.model;
        },

        // Goes through all registered components and updates the model values
        updateModel: function () {
            Object.keys(this.inputs).forEach(function (name) {
                var component = this.inputs[name];
                this.model[name] = component.state._value;
            }.bind(this));
        },

        // Reset each key in the model to the original / initial value
        resetModel: function () {
            Object.keys(this.inputs).forEach(function (name) {
                this.inputs[name].resetValue();
            }.bind(this));
            this.validateForm();
        },

        setInputValidationErrors: function (errors) {
            Object.keys(this.inputs).forEach(function (name, index) {
                var component = this.inputs[name];
                var args = [{
                    _isValid: !(name in errors),
                    _validationError: errors[name]
                }];
                component.setState.apply(component, args);
            }.bind(this));
        },

        // Go through errors from server and grab the components
        // stored in the inputs map. Change their state to invalid
        // and set the serverError message
        updateInputsWithError: function (errors) {
            Object.keys(errors).forEach(function (name, index) {
                var component = this.inputs[name];

                if (!component) {
                    throw new Error('You are trying to update an input that does not exists. Verify errors object with input names. ' + JSON.stringify(errors));
                }

                var args = [{
                    _isValid: false,
                    _externalError: errors[name]
                }];
                component.setState.apply(component, args);
            }.bind(this));
        },

        // Traverse children and its children to find all inputs by checking name prop.
        traverseChildrenAndRegisterInputs: function (children) {

            // If not an component (text, ...) skip
            if (typeof children !== 'object' || children === null) {
                return children;
            }
            return React.Children.map(children, function (child) {

                // If not an component (text, ...) skip
                if (typeof child !== 'object' || child === null) {
                    return child;
                }

                // check input has a name attribute
                if (child.props && child.props.name) {

                    return React.cloneElement(child, {
                        _attachToForm: this.attachToForm,
                        _detachFromForm: this.detachFromForm,
                        _validate: this.validate,
                        _isFormDisabled: this.isFormDisabled,
                        _isValidValue: function (component, value) {
                            return this.runValidation(component, value).isValid;
                        }.bind(this)
                    }, child.props && child.props.children);
                } else {
                    // traverse also children of children
                    return React.cloneElement(child, {}, this.traverseChildrenAndRegisterInputs(child.props && child.props.children));
                }
            }, this);

        },

        isFormDisabled: function () {
            return this.props.disabled;
        },

        getCurrentValues: function () {
            return Object.keys(this.inputs).reduce(function (data, name) {
                var component = this.inputs[name];
                data[name] = component.state._value;
                return data;
            }.bind(this), {});
        },

        setFormFresh: function (isFresh) {
            var inputs = this.inputs;
            var inputKeys = Object.keys(inputs);

            // Iterate through each component and set it as "fresh" (untouched) or "dirty".
            inputKeys.forEach(function (name, index) {
                var component = inputs[name];
                component.setState({
                    _isFresh: isFresh
                });
            }.bind(this));
        },

        // Use the bind values and the actual input value to
        // validate the input and set its state. Then check the
        // state of the form itself
        validate: function (component) {
            if (component.state._isFresh === true) {
                return;
            }

            // Trigger onChange
            if (this.state.canChange) {
                this.props.onChange(this.getCurrentValues());
            }

            var validation = this.runValidation(component);
            // Run through the validations, split them up and call
            // the validator IF there is a value or it is required
            component.setState({
                _isValid: validation.isValid,
                _isRequired: validation.isRequired,
                _validationError: validation.error,
                _externalError: null
            }, this.validateForm);

        },

        // Checks validation on current value or a passed value
        runValidation: function (component, value) {

            var currentValues = this.getCurrentValues();
            var validationErrors = component.props.validationErrors;
            var validationError = component.props.validationError;
            value = arguments.length === 2 ? value : component.state._value;

            var validationResults = this.runRules(value, currentValues, component._validations);
            var requiredResults = this.runRules(value, currentValues, component._requiredValidations);

            // the component defines an explicit validate function
            if (typeof component.validate === "function") {
                validationResults.failed = component.validate() ? [] : ['failed'];
            }

            var isRequired = Object.keys(component._requiredValidations).length ? !!requiredResults.success.length : false;
            var isValid = !validationResults.failed.length && !(this.props.validationErrors && this.props.validationErrors[component.props.name]);

            return {
                isRequired: isRequired,
                isValid: isRequired ? false : isValid,
                error: (function () {

                    if (isValid && !isRequired) {
                        return '';
                    }

                    if (validationResults.errors.length) {
                        return validationResults.errors[0];
                    }

                    // Manually passed validationErrors messages from Form component: mapping key(component name):value(message)
                    if (this.props.validationErrors && this.props.validationErrors[component.props.name]) {
                        return this.props.validationErrors[component.props.name];
                    }

                    if (isRequired) {
                        return validationErrors[requiredResults.success[0]] || null;
                    }

                    // If component is invalid find first failed validation message from validation result
                    // and its translation and return || if not found return default validationError msg from component
                    if (!isValid) {
                        return validationErrors[validationResults.failed[0]] || validationError;
                    }

                }.call(this))
            };

        },

        runRules: function (value, currentValues, validations) {

            // validation result
            var validationResults = {
                errors: [],
                failed: [],
                success: []
            };

            if (Object.keys(validations).length) {
                Object.keys(validations).forEach(function (validationMethod) {

                    if (AF.validationRules[validationMethod] && typeof validations[validationMethod] === 'function') {
                        throw new Error('AFForm does not allow you to override default validations: ' + validationMethod);
                    }

                    if (!AF.validationRules[validationMethod] && typeof validations[validationMethod] !== 'function') {
                        throw new Error('AFForm does not have the validation rule: ' + validationMethod);
                    }

                    if (typeof validations[validationMethod] === 'function') {
                        var validation = validations[validationMethod](currentValues, value);
                        if (typeof validation === 'string') {
                            validationResults.errors.push(validation);
                            validationResults.failed.push(validationMethod);
                        } else if (!validation) {
                            validationResults.failed.push(validationMethod);
                        }
                        return;

                    } else if (typeof validations[validationMethod] !== 'function') {
                        var validation = AF.validationRules[validationMethod](currentValues, value, validations[validationMethod]);
                        if (typeof validation === 'string') {
                            validationResults.errors.push(validation);
                            validationResults.failed.push(validationMethod);
                        } else if (!validation) {
                            validationResults.failed.push(validationMethod);
                        } else {
                            validationResults.success.push(validationMethod);
                        }
                        return;
                    }

                    return validationResults.success.push(validationMethod);
                });
            }

            return validationResults;
        },

        // Validate the form by going through all child input components and check their valid state
        validateForm: function () {
            var allIsValid = true;
            var inputs = this.inputs;
            var inputKeys = Object.keys(inputs);

            // We need a callback as we are validating all inputs again. This will
            // run when the last component has set its state
            var onValidationComplete = function () {
                inputKeys.forEach(function (name) {
                    if (!inputs[name].state._isValid) {
                        allIsValid = false;
                    }
                }.bind(this));

                this.setState({
                    isValid: allIsValid
                });

                if (allIsValid) {
                    this.props.onValid();
                } else {
                    this.props.onInvalid();
                }

                // Tell the form that it can start to trigger change events
                this.setState({
                    canChange: true
                });

            }.bind(this);

            // Run validation again in case affected by other inputs. The
            // last component validated will run the onValidationComplete callback
            inputKeys.forEach(function (name, index) {
                var component = inputs[name];

                if (component.state._isFresh === true) {
                    return;
                }

                var validation = this.runValidation(component);
                if (validation.isValid && component.state._externalError) {
                    validation.isValid = false;
                }
                component.setState({
                    _isValid: validation.isValid,
                    _isRequired: validation.isRequired,
                    _validationError: validation.error,
                    _externalError: !validation.isValid && component.state._externalError ? component.state._externalError : null
                }, index === inputKeys.length - 1 ? onValidationComplete : null);
            }.bind(this));

            // If there are no inputs, set state where form is ready to trigger
            // change event. New inputs might be added later
            if (!inputKeys.length && this.isMounted()) {
                this.setState({
                    canChange: true
                });
            }
        },

        // Method put on each input component to register itself to the AFForm
        attachToForm: function (component) {
            this.inputs[component.props.name] = component;
            this.model[component.props.name] = component.state._value;
            this.validate(component);
        },

        // Method put on each input component to unregister itself from the form
        detachFromForm: function (component) {
            delete this.inputs[component.props.name];
            delete this.model[component.props.name];
        },

        render: function () {
            return React.DOM.form({
                onSubmit: this.submit,
                className: this.props.className,
                noValidate: true,
                formNoValidate: true
            }, this.traverseChildrenAndRegisterInputs(this.props.children));
        }
    });

}).apply(window, [window, AF]);
