/*
 This file contains example translations of AF REST server metamodel fields to HTML Inputs.
 Serves as default widge set powered by Twitter Bootstrap with help of React Bootstrap.
 */
;
(function (global, React, ReactBootstrap, AF, Mixin) {

    /**
     * Construct new React Element from given field with given data and cfg.
     *
     * @param metamodel the AF REST metamodel object
     * @param field the AF REST field metamodel object
     * @param data the data which override default values taken from field metamodel for given input
     * @param cfg options
     * @returns constructed React element build from given field metamodel
     */
    AF.createInput = function (metamodel, field, data, cfg) {
        // validation of input parameters is skipped since AF.buildInputs() function should do that

        // Find component by type
        if (!field.tag || CommonInputBuilder.supportedTypes.indexOf(field.tag) >= 0) {
            return CommonInputBuilder.build(metamodel, field, data, cfg);
        }

        if (field.tag === "date" || field.tag === "datetime-local") {
            return DateInputBuilder.build(metamodel, field, data, cfg);
        }

        if (field.tag === "checkbox" && field.type.toString().toLocaleLowerCase() === "boolean") {
            return BooleanCheckboxInputBuilder.build(metamodel, field, data, cfg);
        }

        if (field.tag === "select") {
            return SelectInputBuilder.build(metamodel, field, data, cfg);
        }

        if (field.tag === "radio") {
            return RadioInputBuilder.build(metamodel, field, data, cfg);
        }

        console.log("Ignoring field=" + JSON.stringify(field));
    };

    //*************************************************************************
    // HTML input React elements properties builders
    //*************************************************************************

    function defaultProps(metamodel, field, data, cfg) {
        var type = field.tag;
        var fieldName = AF.Util.buildFieldName(field.name, field.prefix);
        var value = data[fieldName] ? data[fieldName] : field.value;
        var label = field.label ? field.label : AF.Util.capitalizeFirstLetter(field.name);

        var props = {
            label: label,
            value: value,
            name: fieldName,
            type: type,
            css: {
                groupClassName: cfg.css && cfg.css.groupClassName ? cfg.css.groupClassName : 'group-class',
                wrapperClassName: cfg.css && cfg.css.wrapperClassName ? cfg.css.wrapperClassName : 'wrapper-class',
                //wrapperClassName: cfg.css && cfg.css.wrapperClassName ? cfg.css.wrapperClassName : 'col-xs-10',
                labelClassName: cfg.css && cfg.css.labelClassName ? cfg.css.labelClassName : 'label-class'
                //labelClassName: cfg.css && cfg.css.labelClassName ? cfg.css.labelClassName : 'col-xs-2'
            }
        };

        // Set React key for this Input
        props.key = fieldName;

        _defaultRules(field, props, metamodel, data, cfg);
        _defaultProperties(field, props, metamodel, data, cfg);

        // set field readOnly
        if (cfg.readOnly && (cfg.readOnly === true || (cfg.readOnly.constructor === Array && cfg.readOnly.indexOf(fieldName) >= 0))) {
            props.readOnly = true;
        }

        return props;
    }

    function _defaultRules(field, props, metamodel, data, cfg) {
        if (field.constraints && typeof field.constraints === 'object') {
            var c = field.constraints;

            if (c.required && (c.required === "true" || c.required === true)) {
                props.required = true;
            }

            if (c.readOnly && c.readOnly == true) {
                props.readOnly = true;
            } else {
                props.readOnly = false;
            }

            var rules = {};
            props.validations = rules;
            props.validationErrors = {
                isDefaultRequiredValue: AF.Util.formatString(AF.validationRules.translations.isDefaultRequiredValue)
            };

            if (field.tag === "number") {
                rules.isNumeric = null;
                props.validationErrors.isNumeric = AF.Util.formatString(AF.validationRules.translations.isNumeric);
            }

            if (c.maxLength) {
                rules.maxLength = c.maxLength;
                props.validationErrors.maxLength = AF.Util.formatString(AF.validationRules.translations.maxLength, rules.maxLength);
            }
            if (c.minLength) {
                rules.minLength = c.minLength;
                props.validationErrors.minLength = AF.Util.formatString(AF.validationRules.translations.minLength, rules.minLength);
            }
            if (c.minValue) {
                rules.minValue = parseInt(c.minValue, 10);
                props.validationErrors.minValue = AF.Util.formatString(AF.validationRules.translations.minValue, rules.minValue);
            }
            if (c.maxValue) {
                rules.maxValue = parseInt(c.maxValue, 10);
                props.validationErrors.maxValue = AF.Util.formatString(AF.validationRules.translations.maxValue, rules.maxValue);
            }

            if (c.email) {
                rules.isEmail = true;
                props.validationErrors.isEmail = AF.Util.formatString(AF.validationRules.translations.isEmail);
            }
        }
    }

    function _defaultProperties(field, props, metamodel, data, cfg) {
        if (field.properties && typeof field.properties === 'object') {
            if (field.properties.placeholder) {
                props.placeholder = field.properties.placeholder;
            }

            if (field.properties.multiple && (field.properties.multiple === true || field.properties.multiple === "true")) {
                props.multiple = true;
            }
        }
    };

    var CommonInputBuilder = {
        supportedTypes: ["text", "number", "password", "email", "hidden"],

        build: function (metamodel, field, data, cfg) {
            var props = defaultProps(metamodel, field, data, cfg);

            // Override to type=text
            if (!field.tag || field.tag === "number") {
                props.type = "text";
            }

            // call callback if any
            cfg.fieldCallback(props, field);

            return <CommonInput {...props} />;
        }
    };

    var DateInputBuilder = {
        build: function (metamodel, field, data, cfg) {
            var props = defaultProps(metamodel, field, data, cfg);

            // Set date value transformer
            if (props.type === "datetime-local") {
                props.valueTransformer = AF.valueTransformers.dateTime;
            } else {
                props.valueTransformer = AF.valueTransformers.date
            }

            // call callback if any
            cfg.fieldCallback(props, field);

            return <CommonInput {...props} />;
        }
    };

    var BooleanCheckboxInputBuilder = {
        build: function (metamodel, field, data, cfg) {
            var props = defaultProps(metamodel, field, data, cfg);

            if (typeof props.value !== "boolean") {
                props.value = props.value === "true" ? true : false;
            }

            // if required change validation rule
            if (props.required === true) {
                props.required = {isFalse: true};
                props.validationErrors.isFalse = AF.Util.formatString(AF.validationRules.translations.isFalse);
            }

            // call callback if any
            cfg.fieldCallback(props, field);

            return <BooleanCheckboxInput {...props} />;
        }
    };

    var SelectInputBuilder = {

        build: function (metamodel, field, data, cfg) {
            var props = defaultProps(metamodel, field, data, cfg);

            props.options = field.options;
            if (!props.multiple) {
                props.multiple = false;
            }

            // call callback if any
            cfg.fieldCallback(props, field);

            return <SelectInput {...props} />;
        }
    };

    var RadioInputBuilder = {

        build: function (metamodel, field, data, cfg) {
            var props = defaultProps(metamodel, field, data, cfg);

            props.options = field.options;

            // call callback if any
            cfg.fieldCallback(props, field);

            return <RadioInput {...props} />;
        }
    };

    //***********************************************************************//
    // AF REST default React Widget set of HTML INPUT elements used by builders.
    //***********************************************************************//

    var CommonInput = React.createClass({
        mixins: [Mixin],

        getDefaultProps: function () {
            return {
                valueTransformer: AF.valueTransformers.default
            };
        },

        changeValue: function (e) {
            // use bootstrap getValue()
            var inputValue = this.refs.input.getValue();
            var transformed = this.props.valueTransformer.fromInput(inputValue);
            this.setValue(transformed);
        },

        getValueConverted: function () {
            return this.props.valueTransformer.toInput(this.state._value);
        },

        render: function () {
            var bsStyle = {};
            if (this.showError()) {
                bsStyle.bsStyle = "error";
            }

            return (
                <ReactBootstrap.Input
                    key={this.props.key}
                    ref="input"
                    onChange={this.changeValue}

                    label={this.props.label}
                    value={this.getValueConverted()}
                    name={this.props.name}
                    type={this.props.type || 'text'}

                    placeholder={this.props.placeholder}

                    help={this.getErrorMessage()}
                    {...bsStyle}
                    hasFeedback

                    readOnly={this.props.readOnly}

                    groupClassName={this.props.css.groupClassName}
                    wrapperClassName={this.props.css.wrapperClassName}
                    labelClassName={this.props.css.labelClassName}
                    noValidate
                    />
            );
        }
    });

    var BooleanCheckboxInput = React.createClass({
        mixins: [Mixin],

        changeValue: function (evt) {
            // use React Bootstrap getChecked()
            var value = this.refs.input.getChecked();
            this.setValue(value);
        },

        render: function () {
            var bsStyle = {};
            if (this.showError()) {
                bsStyle.bsStyle = "error";
            }

            return (
                <ReactBootstrap.Input
                    key={this.props.key}
                    ref="input"
                    onChange={this.changeValue}

                    label={this.props.label}
                    value={this.getValue()}
                    checked={this.getValue()}
                    name={this.props.name}
                    type="checkbox"

                    placeholder={this.props.placeholder}

                    help={this.getErrorMessage()}
                    {...bsStyle}
                    hasFeedback

                    disabled={this.props.readOnly}

                    groupClassName={this.props.css.groupClassName}
                    wrapperClassName={this.props.css.wrapperClassName}
                    labelClassName={this.props.css.labelClassName}
                    noValidate
                    />
            );
        }
    });

    var SelectInput = React.createClass({
        mixins: [Mixin],

        getDefaultProps: function () {
            return {
                valueTransformer: AF.valueTransformers.noOp
            };
        },

        changeValue: function (e) {
            // use bootstrap getValue()
            var value = this.refs.input.getValue();
            if (value === "___empty_option___") {
                value = undefined;
            }
            if (this.props.multiple && value.length === 1 && value[0] === "___empty_option___") {
                value = [];
            }

            var transformed = this.props.valueTransformer.fromInput(value);
            this.setValue(transformed);
        },

        getValueConverted: function () {
            return this.props.valueTransformer.toInput(this.state._value);
        },

        render: function () {

            var otherProps = {};
            if (this.props.multiple === true) {
                otherProps.multiple = true;
            }
            if (this.props.readOnly === true) {
                otherProps.disabled = true;
            }

            var options = [];
            if (true) {
                var props = {
                    value: '___empty_option___',
                    key: "___empty_option___"
                };
                options.push(<option {...props}>...</option>);
            }
            if (this.props.options && typeof this.props.options === "object") {
                Object.keys(this.props.options).forEach(function (label) {
                    var optionValue = this.props.options[label];
                    options.push(<option key={optionValue} value={optionValue}>{label}</option>)
                }.bind(this));
            }

            var bsStyle = {};
            if (this.showError()) {
                bsStyle.bsStyle = "error";
            }

            return (
                <ReactBootstrap.Input
                    key={this.props.key}
                    ref="input"
                    onChange={this.changeValue}

                    label={this.props.label}
                    value={this.getValueConverted()}
                    name={this.props.name}
                    type="select"

                    help={this.getErrorMessage()}
                    {...bsStyle}

                    {...otherProps}

                    groupClassName={this.props.css.groupClassName}
                    wrapperClassName={this.props.css.wrapperClassName}
                    labelClassName={this.props.css.labelClassName}
                    noValidate>

                    {options}

                </ReactBootstrap.Input>
            );
        }
    });

    var RadioInput = React.createClass({
        mixins: [Mixin],

        changeValue: function (e) {
            // use bootstrap getValue()
            var value = e.target.value;
            this.setValue(value);
        },

        render: function () {

            var otherProps = {};
            if (this.props.readOnly === true) {
                otherProps.disabled = true;
            }

            var options = [];
            if (!this.props.options || typeof this.props.options !== "object") {
                throw new Error("Could not create HTML select without options!");
            }

            Object.keys(this.props.options).forEach(function (label) {
                var optionValue = this.props.options[label];

                options.push(
                    <div className="radio-inline">
                        <label>
                            <input
                                key={optionValue}
                                value={optionValue}
                                name={this.props.name}
                                type="radio"
                                onChange={this.changeValue}
                                checked={this.getValue() === optionValue}
                                noValidate/>
                            {label}
                        </label>
                    </div>
                );
            }.bind(this));

            var formGroupClassName = "form-group " + this.props.css.groupClassName + (this.isValid() ? "" : " has-error");
            var labelClassName = "control-label " + this.props.css.labelClassName;
            var wrapperClassName = this.props.css.wrapperClassName;

            return (
                <div className={formGroupClassName} key={this.props.key}>
                    <label className={labelClassName}>
                        <span>{this.props.label}</span>
                    </label>

                    <div className={wrapperClassName}>
                        {options}
                    </div>
                </div>
            );
        }
    });

}).apply(window, [window, React, ReactBootstrap, AF, AF.AFInputMixin]);



