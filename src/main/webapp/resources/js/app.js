/*
 * Sample example main app script.
 */

var app = {};

;
(function (global, document, $, app) {

    var cfg = {
        rootUrl: window.location.origin + "/",
        restUrl: window.location.origin + "/rest/"
    };
    app.cfg = cfg;

    app.Auth = {
        //user: {
        //    "id": 1,
        //    "username": "admin",
        //    "password": "admin",
        //    "email": "prochka6@fel.cvut.cz",
        //    "firstName": "Kamil",
        //    "lastName": "Procházka",
        //    "gender": "MALE",
        //    "roles": [
        //        "ADMIN",
        //        "USER"
        //    ]
        //},
        user: null,
        authenticated: false,

        isAuthenticated: function () {
            return this.authenticated;
        },

        isAdmin: function () {
            if (this.isAuthenticated()) {
                var isAdmin = false;
                app.Auth.user.roles.forEach(function (item) {
                    if ("ADMIN" === item) {
                        isAdmin = true;
                    }
                });
                return isAdmin;
            }

            return false;
        },

        getFullName: function () {
            return this.isAuthenticated() ? app.Auth.user.firstName + " " + app.Auth.user.lastName : "UNAUTHENTICATED";
        }
    };

    // Load user from session storage
    (function () {
        if (!app.Auth.isAuthenticated()) {
            // Load user from session storage
            var userString = sessionStorage.getItem("authenticatedUser");
            if (userString) {
                var user = JSON.parse(userString);
                if (user.username && user.password) {
                    app.Auth.user = user;
                    app.Auth.authenticated = true;

                    if (app.Auth.isAdmin() === true) {
                        $("body").addClass("role-admin");
                    }

                    return;
                }
            }

            // Redirect to login page if not there
            if (document.URL.indexOf("index.html") < 0) {
                window.location = app.cfg.rootUrl + "index.html";
            }
        }
    }).apply(window);

    app.Auth.authenticateUser = function (username, password) {
        $.ajax({
            dataType: "json",
            url: app.cfg.restUrl + "/userDetails",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", BasicHeader(username, password));
            }
        }).done(function (data) {
            if (data) {
                sessionStorage.setItem("authenticatedUser", JSON.stringify(data));
                app.Auth.user = data;
                app.Auth.authenticated = true;
            } else {
                throw Error("Could not load authentication details");
            }
        }).fail(function (e) {
            alert("Could not authenticate!!!" + e);
        });
    };

    app.Auth.clear = function () {
        sessionStorage.removeItem("authenticatedUser");
        // reload the page
        window.location.reload();
    };

    function BasicHeader(username, password) {
        return "Basic " + btoa(username + ":" + password);
    }

    // common AJAX setup
    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.withCredentials = true;
            if (app.Auth.user) {
                xhr.setRequestHeader("Authorization", BasicHeader(app.Auth.user.username, app.Auth.user.password));
            }
        },
        statusCode: {
            401: function () {
                alert("Unauthorized!");
            }
        }
    });
    //

}).apply(window, [window, document, jQuery, app]);
