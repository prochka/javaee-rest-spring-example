$.ajaxSetup({
    beforeSend: function (xhr) {
        xhr.withCredentials = true;
        xhr.setRequestHeader("Authorization", "Basic " + btoa("admin:admin"));
    },
    statusCode: {
        401: function () {
            alert("Unauthorized!");
        }
    }
});


