package cz.kprochazka.af.rest.example.service;

import cz.kprochazka.af.rest.example.model.Person;

/**
 *{@link cz.kprochazka.af.rest.example.model.Person} storage.
 *
 * @author Kamil Prochazka
 */
public interface PersonStorage extends Storage<Person> {

    Person findByUsername(String username);

}

