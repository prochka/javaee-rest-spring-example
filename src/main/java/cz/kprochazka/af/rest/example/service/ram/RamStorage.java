package cz.kprochazka.af.rest.example.service.ram;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import cz.kprochazka.af.rest.example.model.IdGeneratedEntityObject;
import cz.kprochazka.af.rest.example.service.Storage;

/**
 * Simple RAM Storage implementation.
 *
 * <br/> Implementations must be THREAD SAFE !
 *
 * @author Kamil Prochazka
 */
abstract class RamStorage<T extends IdGeneratedEntityObject> implements Storage<T> {

    protected long idGenerator = 0;
    protected Map<Long, T> storage = new LinkedHashMap<>();

    @Override
    public synchronized T find(Long id) {
        return storage.get(id);
    }

    @Override
    public synchronized T saveOrUpdate(T entity) {
        if (entity.getId() == null) {
            entity.setId(generateId());
        }
        storage.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public synchronized boolean remove(Long id) {
        T entity = storage.get(id);
        if (entity != null) {
            storage.remove(entity);
        }
        return entity != null;
    }

    @Override
    public synchronized List<T> list() {
        return new ArrayList<>(storage.values());
    }

    // helper methods
    synchronized long generateId() {
        return ++idGenerator;
    }

}
