package cz.kprochazka.af.rest.example.ws.validator;


import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Date;

import cz.kprochazka.af.rest.example.model.AbsenceInstance;

/**
 * Validator validates that {@link cz.kprochazka.af.rest.example.model.AbsenceInstance} startDate is before endDate.
 *
 * @author Kamil Prochazka
 */
@Component
public class AbsenceDateIntervalValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return AbsenceInstance.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        AbsenceInstance absence = (AbsenceInstance) target;

        Date startDate = absence.getStartDate();
        Date endDate = absence.getEndDate();

        if (startDate == null || endDate == null) {
            return;
        }

        if (endDate.getTime() < (startDate.getTime() + (24 * 60 * 60 * 1000))) {
            errors.rejectValue("endDate", "AbsenceInstance.illegal.dateInterval");
        }
    }
}
