package cz.kprochazka.af.rest.example.service.ram;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Date;

import javax.annotation.PostConstruct;

import cz.kprochazka.af.rest.example.model.Address;
import cz.kprochazka.af.rest.example.model.Country;
import cz.kprochazka.af.rest.example.model.Gender;
import cz.kprochazka.af.rest.example.model.Person;
import cz.kprochazka.af.rest.example.model.UserRole;
import cz.kprochazka.af.rest.example.service.PersonStorage;

/**
 * {@link Person} ram storage implementation with initial default instances.
 *
 * @author Kamil Prochazka
 */
@Repository
class PersonRamStorage extends RamStorage<Person> implements PersonStorage {

    @Autowired
    private CountryRamStorage countryStorage;

    @Override
    public Person findByUsername(String username) {
        if (!StringUtils.hasText(username)) {
            return null;
        }

        for (Person person : list()) {
            if (username.equals(person.getUsername())) {
                return person;
            }
        }

        return null;
    }

    @PostConstruct
    void postConstruct() {
        Country cz = countryStorage.findByCode("CZ");

        Person person1 = saveOrUpdate(new Person("admin", "admin", "Kamil", "Procházka", "prochka6@fel.cvut.cz", new Date(), 25, Gender.MALE));
        person1.setUserRoles(Arrays.asList(UserRole.ADMIN, UserRole.USER));
        person1.setAddress(new Address("Starý Pelhřimov 91", "Pelhřimov", 39301, "Česká Republika"));
        person1.setCountry(cz);

        Person person2 = saveOrUpdate(new Person("tom", "tom", "Martin", "Tomášek", "tom@toms-cz.com", new Date(), 26, Gender.MALE));
        person2.setUserRoles(Arrays.asList(UserRole.USER));
        person2.setAddress(new Address("U Hlavní 1", "Strakonice", 38601, "Česká Republika"));
        person2.setCountry(cz);

        Person person3 = saveOrUpdate(new Person("user", "user", "Caroline", "Smith", "csmith@toms-cz.com", new Date(), 45, Gender.FEMALE));
        person3.setUserRoles(Arrays.asList(UserRole.USER));
        person3.setCountry(cz);
    }

}
