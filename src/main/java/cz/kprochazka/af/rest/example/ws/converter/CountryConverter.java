package cz.kprochazka.af.rest.example.ws.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import cz.kprochazka.af.rest.example.model.Country;
import cz.kprochazka.af.rest.example.service.IFacade;

/**
 * Spring SPI {@link Country} converter.
 *
 * @author Kamil Prochazka
 */
class CountryConverter implements Converter<Number, Country> {

    @Autowired
    private IFacade facade;

    @Override
    public Country convert(Number source) {
        if (source != null) {
            return facade.findCountry(source.longValue());
        }

        return null;
    }
}
