package cz.kprochazka.af.rest.example.ws.util;

import com.codingcrayons.aspectfaces.plugins.j2ee.rest.entity.AFComponentUtils;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.entity.AFEntity;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.entity.AFValidationMessage;

import org.springframework.beans.MutablePropertyValues;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * WebDataBindingHelper
 *
 * @author Kamil Prochazka
 */
public class BindingHelper {

    private final WebDataBinder binder;
    private final MessageSource messageSource;
    private final Locale locale;

    public BindingHelper(WebDataBinder binder, MessageSource messageSource, Locale locale) {
        this.binder = binder;
        this.messageSource = messageSource;
        this.locale = locale;
    }

    public BindingHelper allowedFields(String baseName, AFEntity entity) {
        List<String> fieldNames = AFComponentUtils.getFieldNames(entity, false);
        if (binder.getAllowedFields() != null) {
            fieldNames.addAll(Arrays.asList(binder.getAllowedFields()));
        }
        binder.setAllowedFields(fieldNames.toArray(new String[fieldNames.size()]));
        return this;
    }

    public BindingHelper allowedFields(AFEntity entity) {
        return allowedFields(AFComponentUtils.getFieldNames(entity, false));
    }

    public BindingHelper allowedFields(Collection<String> fieldNames) {
        if (binder.getAllowedFields() != null) {
            fieldNames.addAll(Arrays.asList(binder.getAllowedFields()));
        }
        binder.setAllowedFields(fieldNames.toArray(new String[fieldNames.size()]));
        return this;
    }

    public BindingHelper allowedFields(String... fieldNames) {
        return allowedFields(Arrays.asList(fieldNames));
    }

    public BindingHelper disallowedFields(Collection<String> fieldNames) {
        if (binder.getDisallowedFields() != null) {
            fieldNames.addAll(Arrays.asList(binder.getDisallowedFields()));
        }
        binder.setDisallowedFields(fieldNames.toArray(new String[fieldNames.size()]));
        return this;
    }

    public BindingHelper disallowedFields(String... fieldNames) {
        return disallowedFields(Arrays.asList(fieldNames));
    }

    public BindingHelper requiredFields(Collection<String> fieldNames) {
        if (binder.getRequiredFields() != null) {
            fieldNames.addAll(Arrays.asList(binder.getRequiredFields()));
        }
        binder.setRequiredFields(fieldNames.toArray(new String[fieldNames.size()]));
        return this;
    }

    public BindingHelper requiredFields(String... fieldNames) {
        return requiredFields(Arrays.asList(fieldNames));
    }

    public BindingHelper bindAndValidate(Map<String, Object> inputMap) {
        binder.bind(new MutablePropertyValues(inputMap));
        binder.validate();

        return this;
    }

    public BindingResult getBindingResult() {
        return binder.getBindingResult();
    }

    public boolean hasErrors() {
        return binder.getBindingResult().hasErrors();
    }

    public List<AFValidationMessage> getValidationMessages() {
        if (!hasErrors()) {
            return Collections.emptyList();
        }

        List<AFValidationMessage> validationMessages = new ArrayList<>(getBindingResult().getErrorCount());

        List<ObjectError> allErrors = getBindingResult().getAllErrors();
        for (ObjectError error : allErrors) {
            String code = error.getCode();
            String path = null;
            Object value = null;
            String message = messageSource.getMessage(error, locale);

            if (error instanceof FieldError) {
                path = ((FieldError) error).getField();
                value = ((FieldError) error).getRejectedValue();
            }

            validationMessages.add(new AFValidationMessage(code, path, value, message));
        }

        return validationMessages;
    }

    public BindingHelper addError(ObjectError error) {
        binder.getBindingResult().addError(error);
        return this;
    }

    public WebDataBinder getBinder() {
        return binder;
    }
}
