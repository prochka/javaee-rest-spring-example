package cz.kprochazka.af.rest.example.ws.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.userdetails.User;

import cz.kprochazka.af.rest.example.model.AbsenceType;
import cz.kprochazka.af.rest.example.model.Person;
import cz.kprochazka.af.rest.example.model.UserRole;
import cz.kprochazka.af.rest.example.security.SecurityHelper;
import cz.kprochazka.af.rest.example.service.IFacade;

/**
 * Spring SPI {@link AbsenceType} converter.
 *
 * @author Kamil Prochazka
 */
class AbsenceTypeStringConverter implements Converter<String, AbsenceType> {

    @Autowired
    private IFacade facade;

    @Override
    public AbsenceType convert(String source) {
        if (source != null) {
            long id = Long.parseLong(source);

            User user = SecurityHelper.getAuthenticatedUser();
            if (user == null) {
                return null;
            }

            Person person = facade.findPersonByUsername(user.getUsername());
            AbsenceType absenceType = facade.findAbsenceType(id);
            if (absenceType != null) {
                if (person.getCountry() != null && person.getCountry().equals(absenceType.getCountry())) {
                    return absenceType;
                }

                // otherwise must be an ADMIN!
                if (person.getUserRoles().contains(UserRole.ADMIN)) {
                    return absenceType;
                }
            }
        }

        return null;
    }

}
