package cz.kprochazka.af.rest.example.ws;

/**
 * @author Kamil Prochazka
 */
public abstract class AFRestConstants {

    public static final String ENTITY_INFO_REQUEST_PARAM_NAME = "metamodel";

}
