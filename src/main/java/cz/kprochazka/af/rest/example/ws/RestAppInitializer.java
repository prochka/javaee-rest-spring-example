package cz.kprochazka.af.rest.example.ws;

import com.codingcrayons.aspectfaces.plugins.j2ee.rest.AFRestApplication;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.AFRestApplicationImpl;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.context.SimpleAFRequestParameterAFRestContextPostProcessor;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.el.DefaultELContextFactory;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.el.LoggingELContextListener;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.spring.MessageSourceResourceBundle;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.spring.SpringSecurityAFRestContextPostProcessor;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.spring.el.SpringBeanRestContextELResolver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

/**
 * AFRestApplication configurer. Invoked after the Spring container is initialized.
 *
 * @author Kamil Prochazka
 */
@Component
class RestAppInitializer {

    private static final Logger LOG = LoggerFactory.getLogger(RestAppInitializer.class);

    @Autowired
    private AFRestApplication application;

    @Autowired
    private MessageSource messageSource;

    @PostConstruct
    void setUp() throws Exception {
        // AFWeaver.registerAllAnnotations();

        if (application == null || application instanceof AFRestApplicationImpl) {
            AFRestApplicationImpl app = (AFRestApplicationImpl) application;

            // register Spring bean EL resolver
            app.setELContextFactory(new DefaultELContextFactory(new SpringBeanRestContextELResolver()));

            // add custom context listener
            app.getELContextListeners().add(new LoggingELContextListener());

            // set default locale
            app.setDefaultLocale(new Locale("cs", "CZ"));

            // default timezone
            app.setDefaultTimeZone(TimeZone.getTimeZone("Europe/Prague"));

            // set known configuration names
            app.getContextPostProcessors().add(new SimpleAFRequestParameterAFRestContextPostProcessor(Collections.<String>singleton("rest"), null));

            // populate roles from Spring Security context
            app.getContextPostProcessors().add(new SpringSecurityAFRestContextPostProcessor());

            // set default resource bundle
            ResourceBundle rb = new MessageSourceResourceBundle(messageSource);
            app.getResourceBundles().put("msg", rb);
        } else {
            throw new RuntimeException("AFRestApplicationImpl not present!");
        }

        LOG.info("AFRestApplication initialized!");
    }

}
