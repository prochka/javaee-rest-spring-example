package cz.kprochazka.af.rest.example.model;

import com.codingcrayons.aspectfaces.annotations.UiOrder;
import com.codingcrayons.aspectfaces.annotations.UiRequired;
import com.codingcrayons.aspectfaces.annotations.UiType;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class AbsenceInstance extends EntityObject implements Copyable<AbsenceInstance> {

    private AbsenceType absenceType;

    private Date startDate;
    private Date endDate;

    private AbsenceInstanceState status = AbsenceInstanceState.REQUESTED;

    private int duration;

    private Person affectedPerson;

    @UiRequired
    @UiOrder(value = 20)
    @ManyToOne(fetch = FetchType.LAZY)
    public AbsenceType getAbsenceType() {
        return absenceType;
    }

    public void setAbsenceType(AbsenceType absenceType) {
        this.absenceType = absenceType;
    }

    @UiType(value = "readOnly")
    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @UiRequired
    @UiOrder(value = 0)
    @Temporal(value = TemporalType.DATE)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @UiRequired
    @UiOrder(value = 10)
    @Temporal(value = TemporalType.DATE)
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @UiRequired
    @Enumerated(EnumType.STRING)
    public AbsenceInstanceState getStatus() {
        return status;
    }

    public void setStatus(AbsenceInstanceState status) {
        this.status = status;
    }

    @UiOrder(value = -1)
    @ManyToOne(fetch = FetchType.LAZY)
    public Person getAffectedPerson() {
        return affectedPerson;
    }

    public void setAffectedPerson(Person affectedPerson) {
        this.affectedPerson = affectedPerson;
    }

    /**
     * Computes duration of absence in [startDate - endDate] interval.
     */
    public void recomputeDuration() {
        if (startDate != null && endDate != null) {
            this.duration = (int) ((endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24));
        }
    }

    @Override
    public AbsenceInstance copy() {
        AbsenceInstance copy = new AbsenceInstance();

        copy.id = this.id;
        copy.created = this.created;
        copy.modified = this.modified;

        copy.absenceType = this.absenceType;
        copy.startDate = this.startDate;
        copy.endDate = this.endDate;
        copy.status = this.status;
        copy.duration = this.duration;
        copy.affectedPerson = this.affectedPerson;

        return copy;
    }
}
