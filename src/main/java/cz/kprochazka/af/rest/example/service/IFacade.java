package cz.kprochazka.af.rest.example.service;

import org.springframework.security.access.annotation.Secured;

import java.util.List;

import cz.kprochazka.af.rest.example.model.AbsenceInstance;
import cz.kprochazka.af.rest.example.model.AbsenceType;
import cz.kprochazka.af.rest.example.model.Country;
import cz.kprochazka.af.rest.example.model.Person;

/**
 * Service facade for accessing all business objects.
 *
 * @author Kamil Prochazka
 */
public interface IFacade {

    Country findCountry(Long id);

    Country saveOrUpdateCountry(Country country);

    boolean deleteCountry(Long id);

    List<Country> listCountries();

    AbsenceType findAbsenceType(long id);

    AbsenceType saveOrUpdateAbsenceType(AbsenceType absenceType);

    boolean deleteAbsenceType(Long id);

    List<AbsenceType> listAbsenceTypesByCountry(Long id);

    List<AbsenceType> listAbsenceTypes();

    Person findPerson(long id);

    Person findPersonByUsername(String username);

    Person saveOrUpdatePerson(Person person);

    @Secured("ROLE_ADMIN")
    boolean deletePerson(Long id);

    List<Person> listPersons();

    AbsenceInstance findAbsence(long id);

    AbsenceInstance saveOrUpdateAbsence(AbsenceInstance absenceInstance);

    @Secured("ROLE_ADMIN")
    boolean deleteAbsence(Long id);

    List<AbsenceInstance> listAbsences();

    List<AbsenceInstance> listUserAbsences(String username);
}
