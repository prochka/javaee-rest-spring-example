package cz.kprochazka.af.rest.example.ws.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import cz.kprochazka.af.rest.example.model.Person;
import cz.kprochazka.af.rest.example.service.IFacade;

/**
 * Spring SPI {@link Person} converter.
 *
 * @author Kamil Prochazka
 */
class PersonConverter implements Converter<Number, Person> {

    @Autowired
    private IFacade facade;

    @Override
    public Person convert(Number source) {
        if (source != null) {
            return facade.findPerson(source.longValue());
        }

        return null;
    }

}
