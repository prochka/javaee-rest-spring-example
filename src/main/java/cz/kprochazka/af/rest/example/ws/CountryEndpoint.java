package cz.kprochazka.af.rest.example.ws;

import com.codingcrayons.aspectfaces.plugins.j2ee.rest.builder.AFEntityBuilder;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.entity.AFComponentUtils;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.entity.AFEntity;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.entity.AFValidationMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Map;

import cz.kprochazka.af.rest.example.model.Country;
import cz.kprochazka.af.rest.example.ws.util.BindingHelper;

/**
 * CRUD REST endpoint handler on {@link Country} entity.
 *
 * @author Kamil Prochazka
 */
@RestController
@RequestMapping("/rest/country")
class CountryEndpoint extends BaseAFEndpoint {

    @Autowired
    private Helper helper;

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public HttpEntity list() {
        return ResponseEntity.ok(facade.listCountries());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public HttpEntity get(@PathVariable Long id) {
        Country country = facade.findCountry(id);
        if (country == null) {
            return ResponseEntity.notFound().build();
        }

        AFEntityBuilder entityBuilder = restApplication.getEntityBuilder();
        entityBuilder.id(country.getId()).of(country);

        return ResponseEntity.ok(entityBuilder.build());
    }

    @RequestMapping(method = RequestMethod.POST, value = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity create(ServletWebRequest request, WebDataBinderFactory binderFactory, @RequestBody Map<String, Object> countryInput) throws Exception {
        Country newCountry = new Country();

        AFEntity afEntity = restApplication.getEntityBuilder().of(Country.class).build();
        WebDataBinder binder = binderFactory.createBinder(request, newCountry, "");
        BindingHelper bindingHelper = new BindingHelper(binder, messageSource, restContext.getLocale());
        bindingHelper
            .allowedFields(AFComponentUtils.getFieldNames(afEntity, false))
            .disallowedFields("id")
            .bindAndValidate(countryInput);

        if (bindingHelper.hasErrors()) {
            List<AFValidationMessage> validationMessages = bindingHelper.getValidationMessages();

            AFEntity entity = new AFEntity();
            entity.setName(Country.class.getSimpleName());
            entity.setValidationMessages(validationMessages);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(entity);
        }

        // Create a new Country and return
        Country country = facade.saveOrUpdateCountry(newCountry);

        return ResponseEntity.created(UriComponentsBuilder.fromPath("/rest/country/{id}").buildAndExpand(country.getId()).toUri()).build();
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity update(@PathVariable Long id, ServletWebRequest request, WebDataBinderFactory binderFactory,
                             @RequestBody Map<String, Object> countryInput) throws Exception {
        Country country = facade.findCountry(id);
        if (country == null) {
            return ResponseEntity.notFound().build();
        }

        AFEntityBuilder entityBuilder = restApplication.getEntityBuilder();
        entityBuilder.of(country).id(id);
        AFEntity afEntity = entityBuilder.of(country).build();
        WebDataBinder binder = binderFactory.createBinder(request, country, "");
        BindingHelper bindingHelper = new BindingHelper(binder, messageSource, restContext.getLocale());
        bindingHelper
            .allowedFields(AFComponentUtils.getFieldNames(afEntity, false))
            .disallowedFields("id")
            .bindAndValidate(countryInput);

        if (bindingHelper.hasErrors()) {
            List<AFValidationMessage> validationMessages = bindingHelper.getValidationMessages();

            AFEntity entity = new AFEntity();
            entity.setId(id);
            entity.setName(Country.class.getSimpleName());
            entity.setValidationMessages(validationMessages);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(entity);
        }

        country = facade.saveOrUpdateCountry(country);

        return ResponseEntity.ok(restApplication.getEntityBuilder().of(country).build());
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public HttpEntity delete(Long id) {
        boolean deleted = facade.deleteCountry(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
