package cz.kprochazka.af.rest.example.service;

import java.util.List;

import cz.kprochazka.af.rest.example.model.IdGeneratedEntityObject;

/**
 * Contract for simple CRUD operations.
 *
 * @author Kamil Prochazka
 */
public interface Storage<T extends IdGeneratedEntityObject> {

    T find(Long id);

    T saveOrUpdate(T entity);

    boolean remove(Long id);

    List<T> list();

}
