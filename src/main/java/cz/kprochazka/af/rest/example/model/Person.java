package cz.kprochazka.af.rest.example.model;

import com.codingcrayons.aspectfaces.annotations.UiIgnore;
import com.codingcrayons.aspectfaces.annotations.UiLabel;
import com.codingcrayons.aspectfaces.annotations.UiOrder;
import com.codingcrayons.aspectfaces.annotations.UiPassword;
import com.codingcrayons.aspectfaces.annotations.UiRequired;
import com.codingcrayons.aspectfaces.annotations.UiType;
import com.codingcrayons.aspectfaces.annotations.UiUserRoles;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.annotations.UiOptionLabel;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * Person entity.
 *
 * @author Kamil Prochazka
 */
@Entity
public class Person extends EntityObject implements Copyable<Person> {

    private String firstName;
    private String lastName;

    private String username;
    private String password;
    private String email;

    private Date hireDate;
    private int age;
    private Gender gender;

    private Address address;
    private Country country;

    private List<UserRole> userRoles;
    private boolean confidentialAgreement = true;

    private boolean active = true;

    public Person() {
    }

    public Person(String login, String password, String firstName, String lastName,
                  String email, Date hireDate, int age, Gender gender) {
        this.username = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.hireDate = hireDate;
        this.age = age;
        this.gender = gender;
    }

    @UiOrder(value = 2)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @UiOrder(value = 1)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @UiOrder(value = 3)
    @Min(value = 15)
    @Max(value = 60)
    @UiRequired
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @UiRequired
    @Enumerated(EnumType.STRING)
    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @UiType(value = "confidentialAgreement")
    public boolean getConfidentialAgreement() {
        return confidentialAgreement;
    }

    public void setConfidentialAgreement(boolean confidentialAgreement) {
        this.confidentialAgreement = confidentialAgreement;
    }

    @UiOrder(value = 4)
    @Embedded
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address myAddress) {
        this.address = myAddress;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @NotEmpty
    @UiType("email")
    @Email
    public String getEmail() {
        return email;
    }

    @UiLabel("Entity.Active")
    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Temporal(value = TemporalType.TIMESTAMP)
    public Date getHireDate() {
        return hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    @UiUserRoles("ROLE_ADMIN")
    @Enumerated(EnumType.STRING)
    public List<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<UserRole> userRoles) {
        this.userRoles = userRoles;
    }

    public void addRole(UserRole userRole) {
        if (this.userRoles == null) {
            this.userRoles = new ArrayList<UserRole>();
        }
        if (!this.userRoles.contains(userRole)) {
            this.userRoles.add(userRole);
        }
    }

    @UiOrder(value = -1)
    @UiType(value = "password")
    @UiRequired
    @UiPassword
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @UiOrder(value = -2)
    @UiRequired
    @UiOptionLabel
    public String getUsername() {
        return username;
    }

    public void setUsername(String login) {
        this.username = login;
    }

    @UiIgnore
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public Person copy() {
        Person copy = new Person();

        copy.id = this.id;
        copy.created = this.created;
        copy.modified = this.modified;
        copy.firstName = this.firstName;
        copy.lastName = this.lastName;
        copy.username = this.username;
        copy.password = this.password;
        copy.email = this.email;
        copy.hireDate = this.hireDate;
        copy.age = this.age;
        copy.gender = this.gender;
        copy.address = this.address != null ? this.address.copy() : null;
        copy.country = this.country;
        copy.userRoles = new ArrayList<>(this.userRoles);
        copy.confidentialAgreement = this.confidentialAgreement;
        copy.active = this.active;

        return copy;
    }
}
