package cz.kprochazka.af.rest.example.service;

import com.codingcrayons.aspectfaces.plugins.j2ee.rest.entity.AFOption;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.entity.AFOptionImpl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.kprochazka.af.rest.example.model.AbsenceType;
import cz.kprochazka.af.rest.example.model.Country;
import cz.kprochazka.af.rest.example.model.Person;
import cz.kprochazka.af.rest.example.model.UserRole;
import cz.kprochazka.af.rest.example.security.SecurityHelper;

/**
 * Utility methods for AF REST.
 *
 * @author Kamil Prochazka
 */
@Component
public class AFUtils {

    @Autowired
    private IFacade facade;

    /**
     * Returns list of UserRoles as AFOptions
     */
    public List<AFOption> userRoles() {
        List<AFOption> result = new ArrayList<>();

        for (UserRole userRole : UserRole.values()) {
            result.add(new AFOptionImpl(StringUtils.capitalize(userRole.toString().toLowerCase()), userRole.toString()));
        }

        return result;
    }

    public Object listEnumValues(String enumClass) throws ClassNotFoundException {
        Class<?> enumC = Class.forName(enumClass);

        if (enumC.isEnum()) {
            return enumC.getEnumConstants();
        }

        return Collections.<AFOption>emptyList();
    }

    public List listEntities(String entityType) {

        if (AbsenceType.class.getSimpleName().equals(entityType)) {
            return listAbsenceTypes();
        }

        if (Person.class.getSimpleName().equals(entityType)) {
            return listPersons();
        }

        return Collections.emptyList();
    }

    /**
     * Returns currently authenticated user AbsenceTypes by country where the given authenticated user is registered.
     *
     * @return list of authenticated user AbsenceTypes
     */
    private List<AbsenceType> listAbsenceTypes() {
        User authenticatedUser = SecurityHelper.getAuthenticatedUser();
        // It should not be possible to list AbsenceTypes if the user is not authenticated anyway !
        if (authenticatedUser == null) {
            return Collections.emptyList();
        }

        String username = authenticatedUser.getUsername();
        Person person = facade.findPersonByUsername(username);
        Country country = person.getCountry();
        List<AbsenceType> absenceTypes = facade.listAbsenceTypesByCountry(country.getId());

        return absenceTypes;
    }

    private List<AFOption> listPersons() {
        List<Person> persons = facade.listPersons();

        List<AFOption> options = new ArrayList<>(persons.size());
        for (Person person : persons) {
            String label = person.getFirstName() + " " + person.getLastName() + " (" + person.getUsername() + ")";
            options.add(new AFOptionImpl(label, Long.toString(person.getId())));
        }

        return options;
    }

}
