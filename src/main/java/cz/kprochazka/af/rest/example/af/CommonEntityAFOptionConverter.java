package cz.kprochazka.af.rest.example.af;

import com.codingcrayons.aspectfaces.plugins.j2ee.rest.builder.AFOptionConverter;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.entity.AFOption;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.entity.AFOptionImpl;

import cz.kprochazka.af.rest.example.model.EntityObject;

/**
 * Common EntityObject AFOptionConverter.
 *
 * @author Kamil Prochazka
 */
public class CommonEntityAFOptionConverter implements AFOptionConverter<EntityObject> {

    @Override
    public boolean canConvert(Object item) {
        return item != null && item.getClass().isAssignableFrom(EntityObject.class);
    }

    @Override
    public AFOption convert(EntityObject item) {
        return new AFOptionImpl(item.getClass().getSimpleName() + "." + item.getId(), String.valueOf(item.getId()));
    }
}
