package cz.kprochazka.af.rest.example.ws.converter;

import org.springframework.core.convert.converter.Converter;

import java.util.Date;

/**
 * Spring SPI converter to convert number value to Date value.
 *
 * @author Kamil Prochazka
 */
class LongToDateConverter implements Converter<Number, Date> {

    @Override
    public Date convert(Number source) {
        if (source != null) {
            return new Date(source.longValue());
        }

        return null;
    }

}
