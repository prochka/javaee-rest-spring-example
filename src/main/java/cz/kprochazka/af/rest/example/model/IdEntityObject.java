package cz.kprochazka.af.rest.example.model;

import com.codingcrayons.aspectfaces.annotations.UiIgnore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public abstract class IdEntityObject implements Serializable {

    private static final long serialVersionUID = 5553924130305731516L;

    /**
     * The version counter used for Hibernate's <em>managed versioning</em>.
     * Every entity has a version counter.
     * The version counter increments only during data modification conflicts.
     *
     * <p/>
     *
     * Consider the following example scenario.
     * Suppose the entity is in use in two application-level transactions
     * by two users at the same time.
     * Each user has modified his instance of the entity.
     * The first user saves his changes, his version counter is incremented,
     * and the changes (and the version number) are persisted to the database.
     * The second user's instance is now <em>dirty</em>:
     * when she saves her changes, the persistence will fail,
     * because the version number of her instance does not match
     * the version number in the database.
     * See the Hibernate documentation on <em>managed versioning</em>
     * or <em>optimistic locking</em> for more information.
     */
    protected Integer version = 0;

    @Transient
    @UiIgnore
    public abstract Long getId();

    public static <T extends Enum<T>> List<Enum<T>> getEnumList(Class<T> clazz) {
        List<Enum<T>> types = new ArrayList<Enum<T>>(Arrays.asList(clazz.getEnumConstants()));
        return types;
    }

    @SuppressWarnings("unchecked")
    public static <T extends Enum<T>> List<Enum<T>> getEnumList(Class<T> clazz, Enum<T>... filter) {
        List<Enum<T>> types = new ArrayList<Enum<T>>();
        for (T value : clazz.getEnumConstants()) {
            boolean filtered = false;
            for (Enum<T> filteredEnum : filter) {
                if (filteredEnum.toString().equals(value.toString())) {
                    filtered = true;
                    break;
                }
            }
            if (!filtered) {
                types.add((T) value);
            }
        }
        return types;
    }

}
