package cz.kprochazka.af.rest.example.ws;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * Custom {@link ObjectMapper} configuration.
 *
 * @author Kamil Prochazka
 */
class CustomObjectMapper extends ObjectMapper {

    public CustomObjectMapper() {
        // DO return NULL values
        // setSerializationInclusion(JsonInclude.Include.NON_NULL);

        // DO NOT return Map null key:value pairs !
        configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);

        // DO indent output
        enable(SerializationFeature.INDENT_OUTPUT);

        // DO write Enum using toString() method.
        enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);

        // DO NOT fail on empty bean
        disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

        // DO write dates as timestamps
        enable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        // write only not null values and not empty
        setSerializationInclusion(JsonInclude.Include.NON_NULL);
        setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
    }

}
