package cz.kprochazka.af.rest.example.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * This enum holds all user role in application.
 *
 * @author Kamil Prochazka
 */
public enum UserRole {

    ADMIN("ROLE_ADMIN"),
    USER("ROLE_USER");

    private final String role;
    private final GrantedAuthority grantedAuthority;

    private UserRole(String role) {
        this.role = role;
        this.grantedAuthority = new SimpleGrantedAuthority(role);
    }

    public GrantedAuthority getGrantedAuthority() {
        return grantedAuthority;
    }

    public boolean equalsRole(String otherRole) {
        return (otherRole == null) ? false : role.equals(otherRole);
    }

    public String toString() {
        return name();
    }

}
