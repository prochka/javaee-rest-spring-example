package cz.kprochazka.af.rest.example.service;

import java.util.List;

import cz.kprochazka.af.rest.example.model.AbsenceInstance;

/**
 * {@link cz.kprochazka.af.rest.example.model.AbsenceInstance} storage.
 *
 * @author Kamil Prochazka
 */
public interface AbsenceInstanceStorage extends Storage<AbsenceInstance> {

    List<AbsenceInstance> listByPerson(Long personId);

}
