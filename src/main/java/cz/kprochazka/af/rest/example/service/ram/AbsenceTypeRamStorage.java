package cz.kprochazka.af.rest.example.service.ram;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import cz.kprochazka.af.rest.example.model.AbsenceType;
import cz.kprochazka.af.rest.example.model.Country;
import cz.kprochazka.af.rest.example.service.AbsenceTypeStorage;

/**
 * {@link AbsenceType} ram storage implementation with initial default instances.
 *
 * @author Kamil Prochazka
 */
@Repository
class AbsenceTypeRamStorage extends RamStorage<AbsenceType> implements AbsenceTypeStorage {

    @Autowired
    private CountryRamStorage countryStorage;

    @Override
    public List<AbsenceType> listByCountry(long id) {
        List<AbsenceType> result = new ArrayList<>();

        for (AbsenceType absenceType : list()) {
            if (absenceType.getCountry() != null && absenceType.getCountry().getId() == id) {
                result.add(absenceType);
            }
        }

        return result;
    }

    @PostConstruct
    void postConstruct() {
        Country cz = countryStorage.findByCode("CZ");

        saveOrUpdate(new AbsenceType("Dovolená", 20, cz));
        saveOrUpdate(new AbsenceType("5-ti týdenní dovolená", 25, cz));
        saveOrUpdate(new AbsenceType("Stěhování", 1, cz));
    }

}
