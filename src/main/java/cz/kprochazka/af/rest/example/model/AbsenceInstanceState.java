package cz.kprochazka.af.rest.example.model;

public enum AbsenceInstanceState {

    REQUESTED("AbsenceInstances.requested"),
    ACCEPTED("AbsenceInstances.accepted"),
    CANCELLED("AbsenceInstances.cancelled"),
    DENIED("AbsenceInstances.denied");

    private final String name;

    private AbsenceInstanceState(String name) {
        this.name = name;
    }

    public boolean equalsName(String otherName) {
        return (otherName == null) ? false : name.equals(otherName);
    }

    public String toString() {
        return name();
    }

}
