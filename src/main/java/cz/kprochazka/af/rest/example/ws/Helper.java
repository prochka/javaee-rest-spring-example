package cz.kprochazka.af.rest.example.ws;

import com.codingcrayons.aspectfaces.plugins.j2ee.rest.AFRestApplication;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.builder.AFEntityBuilder;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.builder.AFEntityBuilderImpl;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.context.AFRestContext;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.entity.AFEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import cz.kprochazka.af.rest.example.model.Person;
import cz.kprochazka.af.rest.example.security.SecurityHelper;
import cz.kprochazka.af.rest.example.service.IFacade;

/**
 * @author Kamil Prochazka
 */
@Component
public class Helper {

    @Autowired
    private IFacade facade;

    @Autowired
    private AFRestApplication application;

    @Autowired
    private AFRestContext context;

    public AFEntity invoke(Long id) {
        User authenticatedUser = SecurityHelper.getAuthenticatedUser();

        AFEntityBuilder entityBuilder = (AFEntityBuilderImpl) application.getEntityBuilder();

        Person person = facade.findPerson(1L);
//        facade.saveOrUpdatePerson(person);

        long start = System.currentTimeMillis();
        AFEntity entity = entityBuilder
//            .id(Arrays.asList(1L, 2L, 3L, 4L))
            .id(person.getId())
            .of(person)
//            .of("instance", facade.findAbsence(1L))
//            .of("type", facade.findAbsenceType(1L))
//            .of(facade.findCountry(1L))
//            .of("c1", country)
//            .of("p2", person)
//            .of("c2", country)
            .build();
        System.out.println("TIME:" + (System.currentTimeMillis() - start) + "ms");

        return new AFEntity();
    }

}
