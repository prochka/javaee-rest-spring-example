package cz.kprochazka.af.rest.example.model;

import com.codingcrayons.aspectfaces.annotations.UiLabel;
import com.codingcrayons.aspectfaces.annotations.UiOrder;
import com.codingcrayons.aspectfaces.annotations.UiRequired;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.annotations.UiOptionLabel;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class AbsenceType extends EntityObject implements Copyable<AbsenceType> {

    private String name;
    private int maxDaysPerYear;
    private Country country;
    private boolean active = true;

    public AbsenceType() {
    }

    public AbsenceType(String name, int maxDaysPerYear, Country country) {
        this.name = name;
        this.maxDaysPerYear = maxDaysPerYear;
        this.country = country;
    }

    @UiOptionLabel
    @UiOrder(value = 1)
    @UiRequired
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @UiRequired
    @UiOrder(value = 4)
    @ManyToOne
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @UiOrder(value = 2)
    @UiRequired
    public int getMaxDaysPerYear() {
        return maxDaysPerYear;
    }

    public void setMaxDaysPerYear(int maxDaysPerYear) {
        this.maxDaysPerYear = maxDaysPerYear;
    }

    @UiLabel("Entity.Active")
    @UiOrder(value = 3)
    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public AbsenceType copy() {
        AbsenceType copy = new AbsenceType();

        copy.id = this.id;
        copy.created = this.created;
        copy.modified = this.modified;
        copy.active = this.active;

        copy.name = this.name;
        copy.maxDaysPerYear = this.maxDaysPerYear;
        copy.country = this.country;

        return copy;
    }
}
