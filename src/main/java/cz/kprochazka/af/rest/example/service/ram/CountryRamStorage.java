package cz.kprochazka.af.rest.example.service.ram;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;

import cz.kprochazka.af.rest.example.model.Country;
import cz.kprochazka.af.rest.example.service.CountryStorage;

/**
 * {@link Country} ram storage implementation with initial default instances.
 *
 * @author Kamil Prochazka
 */
@Repository
class CountryRamStorage extends RamStorage<Country> implements CountryStorage {

    public Country findByCode(String code) {
        if (!StringUtils.hasText(code)) {
            return null;
        }

        for (Country country : list()) {
            if (code.equals(country.getCode())) {
                return country;
            }
        }

        return null;
    }

    @PostConstruct
    void postConstruct() {
        saveOrUpdate(new Country("CZ", "Czech Republic"));
        saveOrUpdate(new Country("SK", "Slovakia"));
        saveOrUpdate(new Country("DE", "Germany"));
        saveOrUpdate(new Country("GB", "United States"));
        saveOrUpdate(new Country("US", "United Kingdom"));
    }

}
