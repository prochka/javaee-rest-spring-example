package cz.kprochazka.af.rest.example.security;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * Spring Security helper class.
 *
 * @author Kamil Prochazka
 */
public final class SecurityHelper {

    private SecurityHelper() {
    }

    /**
     * Gets currently authenticated User or {@code null}.
     *
     * @return the currently authenticated user or {@code null}
     */
    public static User getAuthenticatedUser() {
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication authentication = context.getAuthentication();
        if (authentication != null && !(authentication instanceof AnonymousAuthenticationToken)) {
            return (User) authentication.getPrincipal();
        }

        return null;
    }

    /**
     * {@code true} if current user is admin.
     *
     * @return {@code true} if currently authenticated user is admin
     */
    public static boolean isAdmin() {
        User user = getAuthenticatedUser();
        if (user != null) {
            Collection<GrantedAuthority> authorities = user.getAuthorities();
            for (GrantedAuthority authority : authorities) {
                if ("ROLE_ADMIN".equals(authority.getAuthority())) {
                    return true;
                }
            }

        }

        return false;
    }

}
