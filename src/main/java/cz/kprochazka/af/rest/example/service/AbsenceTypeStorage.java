package cz.kprochazka.af.rest.example.service;

import java.util.List;

import cz.kprochazka.af.rest.example.model.AbsenceType;

/**
 * {@link cz.kprochazka.af.rest.example.model.AbsenceType} storage.
 *
 * @author Kamil Prochazka
 */
public interface AbsenceTypeStorage extends Storage<AbsenceType> {

    List<AbsenceType> listByCountry(long id);

}
