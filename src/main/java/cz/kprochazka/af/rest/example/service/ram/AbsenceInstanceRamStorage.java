package cz.kprochazka.af.rest.example.service.ram;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;

import cz.kprochazka.af.rest.example.model.AbsenceInstance;
import cz.kprochazka.af.rest.example.model.AbsenceInstanceState;
import cz.kprochazka.af.rest.example.model.AbsenceType;
import cz.kprochazka.af.rest.example.model.Person;
import cz.kprochazka.af.rest.example.service.AbsenceInstanceStorage;

/**
 * {@link AbsenceInstance} ram storage implementation with initial default instances.
 *
 * @author Kamil Prochazka
 */
@Repository
class AbsenceInstanceRamStorage extends RamStorage<AbsenceInstance> implements AbsenceInstanceStorage {

    @Autowired
    private PersonRamStorage personStorage;

    @Autowired
    private AbsenceTypeRamStorage absenceTypeStorage;

    @Override
    public List<AbsenceInstance> listByPerson(Long personId) {
        List<AbsenceInstance> absences = list();

        List<AbsenceInstance> result = new ArrayList<>();
        for (AbsenceInstance absence : absences) {
            if (absence.getAffectedPerson() != null && absence.getAffectedPerson().getId().equals(personId)) {
                result.add(absence);
            }
        }

        return result;
    }

    @PostConstruct
    void postConstruct() {
        Person tom = personStorage.findByUsername("tom");
        AbsenceType holiday = absenceTypeStorage.find(1L);
        AbsenceType moving = absenceTypeStorage.find(3L);

        AbsenceInstance absence = new AbsenceInstance();
        absence.setAbsenceType(holiday);
        absence.setAffectedPerson(tom);
        absence.setStatus(AbsenceInstanceState.REQUESTED);

        Calendar instance = GregorianCalendar.getInstance();
        instance.add(Calendar.DATE, -5);

        absence.setStartDate(instance.getTime());
        absence.setEndDate(new Date());
        absence.recomputeDuration();

        saveOrUpdate(absence);

        absence = new AbsenceInstance();
        absence.setAbsenceType(moving);
        absence.setAffectedPerson(tom);
        absence.setStatus(AbsenceInstanceState.ACCEPTED);

        instance = GregorianCalendar.getInstance();
        instance.add(Calendar.DATE, -1);
        absence.setStartDate(instance.getTime());
        absence.setEndDate(new Date());
        absence.recomputeDuration();

        saveOrUpdate(absence);
    }

}
