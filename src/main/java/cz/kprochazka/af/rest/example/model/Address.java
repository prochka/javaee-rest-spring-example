package cz.kprochazka.af.rest.example.model;

import com.codingcrayons.aspectfaces.annotations.UiOrder;
import com.codingcrayons.aspectfaces.annotations.UiRequired;
import com.codingcrayons.aspectfaces.annotations.UiType;

import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class Address implements Serializable, Copyable<Address> {

    private String street;
    private String city;
    private int postCode;
    private String country;

    public Address() {
    }

    public Address(String street, String city, int postCode, String country) {
        this.street = street;
        this.city = city;
        this.postCode = postCode;
        this.country = country;
    }

    @NotEmpty
    @UiRequired
    @UiOrder(value = 1)
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @UiRequired
    @UiOrder(value = 3)
    public int getPostCode() {
        return postCode;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }

    @UiRequired
    @UiOrder(value = 2)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @UiRequired
    @UiType(value = "country")
    @UiOrder(value = 4)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public Address copy() {
        Address copy = new Address();

        copy.street = this.street;
        copy.city = this.city;
        copy.postCode = this.postCode;
        copy.country = this.country;

        return copy;
    }
}
