package cz.kprochazka.af.rest.example.ws;

import com.codingcrayons.aspectfaces.plugins.j2ee.rest.builder.AFEntityBuilder;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.entity.AFComponentUtils;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.entity.AFEntity;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.entity.AFValidationMessage;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Map;

import cz.kprochazka.af.rest.example.model.Address;
import cz.kprochazka.af.rest.example.model.Person;
import cz.kprochazka.af.rest.example.ws.util.BindingHelper;

/**
 * CRUD REST endpoint handler on {@link Person} entity.
 *
 * @author Kamil Prochazka
 */
@RestController
@RequestMapping("/rest/person")
class PersonEndpoint extends BaseAFEndpoint {

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public HttpEntity list() {
        return ResponseEntity.ok(facade.listPersons());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public HttpEntity get(@PathVariable Long id) {
        Person person = facade.findPerson(id);
        if (person == null) {
            return ResponseEntity.notFound().build();
        }

        AFEntityBuilder entityBuilder = restApplication.getEntityBuilder();
        entityBuilder.id(person.getId()).of(person).of("address", person.getAddress() != null ? person.getAddress() : new Address()).collate(true);
        AFEntity entity = entityBuilder.build();

        return ResponseEntity.ok(entity);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity create(ServletWebRequest request, WebDataBinderFactory binderFactory, @RequestBody Map<String, Object> personInput) throws Exception {
        Person newPerson = new Person();

        AFEntity address = restApplication.getEntityBuilder().of("address", Address.class).build();

        AFEntity afEntity = restApplication.getEntityBuilder().of(Person.class).build();
        WebDataBinder binder = binderFactory.createBinder(request, newPerson, "");
        BindingHelper bindingHelper = new BindingHelper(binder, messageSource, restContext.getLocale());
        bindingHelper
            .allowedFields(AFComponentUtils.getFieldNames(afEntity, false))
            .allowedFields(AFComponentUtils.getFieldNames(address, true))
            .disallowedFields("id")
            .bindAndValidate(personInput);

        if (bindingHelper.hasErrors()) {
            List<AFValidationMessage> validationMessages = bindingHelper.getValidationMessages();

            AFEntity entity = new AFEntity();
            entity.setName(Person.class.getSimpleName());
            entity.setValidationMessages(validationMessages);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(entity);
        }

        // Create a new Person and return
        Person person = facade.saveOrUpdatePerson(newPerson);

        return ResponseEntity.created(UriComponentsBuilder.fromPath("/rest/person/{id}").buildAndExpand(person.getId()).toUri()).build();
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity update(@PathVariable Long id, ServletWebRequest request, WebDataBinderFactory binderFactory,
                             @RequestBody Map<String, Object> personInput) throws Exception {
        Person person = facade.findPerson(id);
        if (person == null) {
            return ResponseEntity.notFound().build();
        }

        AFEntity address = restApplication.getEntityBuilder().of("address", Address.class).build();

        AFEntity afEntity = restApplication.getEntityBuilder().of(person).id(id).build();
        WebDataBinder binder = binderFactory.createBinder(request, person, "");
        BindingHelper bindingHelper = new BindingHelper(binder, messageSource, restContext.getLocale());
        bindingHelper
            .allowedFields(AFComponentUtils.getFieldNames(afEntity, false))
            .allowedFields(AFComponentUtils.getFieldNames(address, true))
            .disallowedFields("id")
            .bindAndValidate(personInput);

        if (bindingHelper.hasErrors()) {
            List<AFValidationMessage> validationMessages = bindingHelper.getValidationMessages();

            AFEntity entity = new AFEntity();
            entity.setId(id);
            entity.setName(Person.class.getSimpleName());
            entity.setValidationMessages(validationMessages);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(entity);
        }

        person = facade.saveOrUpdatePerson(person);

        return get(person.getId());
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public HttpEntity delete(Long id) {
        boolean deleted = facade.deletePerson(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
