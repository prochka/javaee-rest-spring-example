package cz.kprochazka.af.rest.example.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import cz.kprochazka.af.rest.example.model.Person;
import cz.kprochazka.af.rest.example.model.UserRole;
import cz.kprochazka.af.rest.example.service.PersonStorage;

/**
 * Simple Spring security UserDetailsService implementation with static initializing map.
 *
 * @author Kamil Prochazka
 */
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private PersonStorage personStorage;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Person person = personStorage.findByUsername(username);
        if (person != null) {
            final String personUsername = person.getUsername();
            final String personPassword = person.getPassword();
            final List<UserRole> userRoles = person.getUserRoles();

            List<GrantedAuthority> authorities = new ArrayList<>(userRoles.size());
            for (UserRole role : userRoles) {
                authorities.add(role.getGrantedAuthority());
            }

            return new User(personUsername, personPassword, authorities);
        }

        throw new UsernameNotFoundException(MessageFormat.format("User with username={0} not found!", username));
    }

}
