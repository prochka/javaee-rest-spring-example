package cz.kprochazka.af.rest.example.model;

/**
 * Return copy of this object.
 *
 * @author Kamil Prochazka
 */
public interface Copyable<T> {

    T copy();

}
