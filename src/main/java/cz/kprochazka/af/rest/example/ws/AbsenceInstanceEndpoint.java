package cz.kprochazka.af.rest.example.ws;

import com.codingcrayons.aspectfaces.plugins.j2ee.rest.builder.AFEntityBuilder;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.entity.AFComponentUtils;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.entity.AFEntity;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.entity.AFValidationMessage;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Map;

import cz.kprochazka.af.rest.example.model.AbsenceInstance;
import cz.kprochazka.af.rest.example.model.Person;
import cz.kprochazka.af.rest.example.security.SecurityHelper;
import cz.kprochazka.af.rest.example.ws.util.BindingHelper;
import cz.kprochazka.af.rest.example.ws.validator.AbsenceDateIntervalValidator;

/**
 * CRUD REST endpoint handler on {@link cz.kprochazka.af.rest.example.model.AbsenceInstance} entity.
 *
 * @author Kamil Prochazka
 */
@RestController
@RequestMapping("/rest/absence")
class AbsenceInstanceEndpoint extends BaseAFEndpoint {

    @Autowired
    private AbsenceDateIntervalValidator validator;

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public HttpEntity list(@RequestParam(required = false, value = "username") String username) {
        if (SecurityHelper.isAdmin()) {
            if (StringUtils.isBlank(username)) {
                return ResponseEntity.ok(facade.listAbsences());
            } else {
                return ResponseEntity.ok(facade.listUserAbsences(username));
            }
        }

        return ResponseEntity.ok(facade.listUserAbsences(SecurityHelper.getAuthenticatedUser().getUsername()));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public HttpEntity get(@PathVariable Long id) {
        AbsenceInstance absenceInstance = facade.findAbsence(id);
        if (absenceInstance == null) {
            return ResponseEntity.notFound().build();
        }

        AFEntityBuilder entityBuilder = restApplication.getEntityBuilder();
        entityBuilder.id(absenceInstance.getId()).of(absenceInstance);

        return ResponseEntity.ok(entityBuilder.build());
    }

    @RequestMapping(method = RequestMethod.POST, value = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity create(ServletWebRequest request, WebDataBinderFactory binderFactory, @RequestBody Map<String, Object> absenceInput) throws Exception {
        AbsenceInstance newAbsenceInstance = new AbsenceInstance();

        AFEntity afEntity = restApplication.getEntityBuilder().of(AbsenceInstance.class).build();
        WebDataBinder binder = binderFactory.createBinder(request, newAbsenceInstance, "");
        BindingHelper bindingHelper = new BindingHelper(binder, messageSource, restContext.getLocale());
        bindingHelper
            .allowedFields(AFComponentUtils.getFieldNames(afEntity, false))
            .disallowedFields("id", "duration", "status")
            .bindAndValidate(absenceInput);
        validator.validate(newAbsenceInstance, bindingHelper.getBindingResult());

        if (bindingHelper.hasErrors()) {
            List<AFValidationMessage> validationMessages = bindingHelper.getValidationMessages();

            AFEntity entity = new AFEntity();
            entity.setName(AbsenceInstance.class.getSimpleName());
            entity.setValidationMessages(validationMessages);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(entity);
        }

        // Create a new AbsenceInstance and return
        Person person = facade.findPersonByUsername(SecurityHelper.getAuthenticatedUser().getUsername());
        newAbsenceInstance.setAffectedPerson(person);
        AbsenceInstance absenceInstance = facade.saveOrUpdateAbsence(newAbsenceInstance);

        return ResponseEntity.created(UriComponentsBuilder.fromPath("/rest/absenceType/{id}").buildAndExpand(absenceInstance.getId()).toUri()).build();
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity update(@PathVariable Long id, ServletWebRequest request, WebDataBinderFactory binderFactory,
                             @RequestBody Map<String, Object> absenceInstanceInput) throws Exception {
        AbsenceInstance absenceInstance = facade.findAbsence(id);
        if (absenceInstance == null) {
            return ResponseEntity.notFound().build();
        }

        AFEntityBuilder entityBuilder = restApplication.getEntityBuilder();
        entityBuilder.of(absenceInstance).id(id);
        AFEntity afEntity = entityBuilder.of(absenceInstance).build();
        WebDataBinder binder = binderFactory.createBinder(request, absenceInstance, "");
        BindingHelper bindingHelper = new BindingHelper(binder, messageSource, restContext.getLocale());
        bindingHelper
            .allowedFields(AFComponentUtils.getFieldNames(afEntity, false))
            .disallowedFields("id", "duration")
            .bindAndValidate(absenceInstanceInput);
        validator.validate(absenceInstance, bindingHelper.getBindingResult());

        if (bindingHelper.hasErrors()) {
            List<AFValidationMessage> validationMessages = bindingHelper.getValidationMessages();

            AFEntity entity = new AFEntity();
            entity.setId(id);
            entity.setName(AbsenceInstance.class.getSimpleName());
            entity.setValidationMessages(validationMessages);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(entity);
        }

        absenceInstance = facade.saveOrUpdateAbsence(absenceInstance);

        return ResponseEntity.ok(restApplication.getEntityBuilder().of(absenceInstance).build());
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public HttpEntity delete(Long id) {
        boolean deleted = facade.deleteAbsence(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
