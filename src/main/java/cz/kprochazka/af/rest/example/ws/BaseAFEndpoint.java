package cz.kprochazka.af.rest.example.ws;

import com.codingcrayons.aspectfaces.plugins.j2ee.rest.AFRestApplication;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.context.AFRestContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import cz.kprochazka.af.rest.example.service.IFacade;

/**
 * Base controller defining base REST url contract for all REST endpoints.
 *
 * @author Kamil Prochazka
 */
abstract class BaseAFEndpoint {

    protected final Logger LOG = LoggerFactory.getLogger(getClass());

    @Autowired
    protected MessageSource messageSource;

    @Autowired
    protected IFacade facade;

    @Autowired
    protected AFRestApplication restApplication;

    @Autowired
    protected AFRestContext restContext;

}
