package cz.kprochazka.af.rest.example.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

import cz.kprochazka.af.rest.example.model.AbsenceInstance;
import cz.kprochazka.af.rest.example.model.AbsenceType;
import cz.kprochazka.af.rest.example.model.Country;
import cz.kprochazka.af.rest.example.model.Person;
import cz.kprochazka.af.rest.example.security.SecurityHelper;

/**
 * Transactional implementation of {@link IFacade}.
 *
 * @author Kamil Prochazka
 */
@Service
@Transactional
class Facade implements IFacade {

    private static final Logger LOG = LoggerFactory.getLogger(Facade.class);

    @Autowired
    private CountryStorage countryStorage;

    @Autowired
    private AbsenceTypeStorage absenceTypeStorage;

    @Autowired
    private PersonStorage personStorage;

    @Autowired
    private AbsenceInstanceStorage absenceInstanceStorage;

    @Override
    public Country findCountry(Long id) {
        Country country = countryStorage.find(id);
        if (country != null) {
            return country.copy();
        }
        return country;
    }

    @Secured("ROLE_ADMIN")
    @Override
    public Country saveOrUpdateCountry(Country country) {
        return countryStorage.saveOrUpdate(country);
    }

    @Secured("ROLE_ADMIN")
    @Override
    public boolean deleteCountry(Long id) {
        return countryStorage.remove(id);
    }

    @Override
    public List<Country> listCountries() {
        return countryStorage.list();
    }

    @Override
    public AbsenceType findAbsenceType(long id) {
        AbsenceType absenceType = absenceTypeStorage.find(id);
        if (absenceType != null) {
            return absenceType.copy();
        }
        return absenceType;
    }

    @Secured("ROLE_ADMIN")
    @Override
    public AbsenceType saveOrUpdateAbsenceType(AbsenceType absenceType) {
        return absenceTypeStorage.saveOrUpdate(absenceType);
    }

    @Secured("ROLE_ADMIN")
    @Override
    public boolean deleteAbsenceType(Long id) {
        return absenceTypeStorage.remove(id);
    }

    @Override
    public List<AbsenceType> listAbsenceTypesByCountry(Long id) {
        return absenceTypeStorage.listByCountry(id);
    }

    @Override
    public List<AbsenceType> listAbsenceTypes() {
        return absenceTypeStorage.list();
    }

    @Override
    public Person findPerson(long id) {
        Person person = personStorage.find(id);
        if (person != null) {
            return person.copy();
        }
        return person;
    }

    @Override
    public Person findPersonByUsername(String username) {
        Person person = personStorage.findByUsername(username);
        if (person != null) {
            return person.copy();
        }
        return person;
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN') or (#p.username == principal.username)")
    public Person saveOrUpdatePerson(@P("p") Person person) {
        return personStorage.saveOrUpdate(person);
    }

    @Override
    @Secured("ROLE_ADMIN")
    public boolean deletePerson(Long id) {
        return personStorage.remove(id);
    }

    @Override
    public List<Person> listPersons() {
        return personStorage.list();
    }

    @Override
    public AbsenceInstance findAbsence(long id) {
        AbsenceInstance instance = absenceInstanceStorage.find(id);
        if (SecurityHelper.isAdmin()) {
            return copy(instance);
        }

        Person person = getAuthenticatedPerson();
        if (person != null && instance.getAffectedPerson() != null && instance.getAffectedPerson().getId().equals(person.getId())) {
            return copy(instance);
        }

        return null;
    }

    private AbsenceInstance copy(AbsenceInstance instance) {
        if (instance != null) {
            return instance.copy();
        }
        return instance;
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN') or (#i.affectedPerson.username == principal.username)")
    public AbsenceInstance saveOrUpdateAbsence(@P("i") AbsenceInstance absenceInstance) {
        absenceInstance.recomputeDuration();
        return absenceInstanceStorage.saveOrUpdate(absenceInstance);
    }

    @Override
    @Secured("ROLE_ADMIN")
    public boolean deleteAbsence(Long id) {
        return absenceInstanceStorage.remove(id);
    }

    @Secured("ROLE_ADMIN")
    @Override
    public List<AbsenceInstance> listAbsences() {
        return absenceInstanceStorage.list();
    }

    @Override
    public List<AbsenceInstance> listUserAbsences(String username) {
        Person person = personStorage.findByUsername(username);
        if (person == null) {
            return Collections.emptyList();
        }

        return absenceInstanceStorage.listByPerson(person.getId());
    }

    private Person getAuthenticatedPerson() {
        User user = SecurityHelper.getAuthenticatedUser();
        return personStorage.findByUsername(user.getUsername());
    }

}
