package cz.kprochazka.af.rest.example.model;

import com.codingcrayons.aspectfaces.annotations.UiLabel;
import com.codingcrayons.aspectfaces.annotations.UiOrder;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.annotations.UiOptionLabel;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Entity;

@Entity
public class Country extends EntityObject implements Copyable<Country> {

    private String name;

    /**
     * Country code as defined in ISO 3166
     */
    private String code;

    private boolean active = true;

    public Country() {
    }

    public Country(String code, String name) {
        this.code = code;
        this.name = name;
    }

    @NotBlank
    @Length(min = 2, max = 3)
    @UiOrder(value = 1)
    public String getCode() {
        return code;
    }

    public void setCode(String shortCut) {
        this.code = shortCut;
    }

    @NotBlank
    @Length(max = 50)
    @UiOrder(value = 5)
    @UiOptionLabel
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @UiLabel("Entity.Active")
    @UiOrder(value = 10)
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public Country copy() {
        Country copy = new Country();

        copy.id = this.id;
        copy.created = this.created;
        copy.modified = this.modified;
        copy.name = this.name;
        copy.code = this.code;
        copy.active = this.active;

        return copy;
    }
}
