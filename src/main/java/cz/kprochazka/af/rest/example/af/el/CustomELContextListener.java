package cz.kprochazka.af.rest.example.af.el;

import java.util.HashMap;

import javax.el.ELContext;
import javax.el.ELContextEvent;
import javax.el.ELContextListener;

/**
 * Simple implementation which expose {@linkplain java.util.Map} for {@link CustomELResolver}.
 *
 * @author Kamil Prochazka
 */
public class CustomELContextListener implements ELContextListener {

    @Override
    public void contextCreated(ELContextEvent ece) {
        ELContext elContext = ece.getELContext();
        elContext.putContext(CustomELResolver.class, new HashMap<String, Object>());
    }

}
