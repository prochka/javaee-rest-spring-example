package cz.kprochazka.af.rest.example.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * A business object (entity) which is persisted in the database.
 * This class is extended by all business objects that are persisted in
 * the database.
 */
@MappedSuperclass
public abstract class IdGeneratedEntityObject extends IdEntityObject {

    private static final long serialVersionUID = -515478233219927119L;

    /**
     * The unique ID of the entity.
     * This is equal to the record ID
     * of the entity's corresponding database record.
     * This field is null if the entity does not exist in the database
     * and has not yet been persisted.
     *
     * <p/>
     *
     * If multiple instances of this class exist with the same ID,
     * and if the ID is not null,
     * the instances correspond to the same entity and database record.
     */
    protected Long id;

    /**
     * Returns the {@link #id}.
     *
     * @return the {@link #id}.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * Sets the {@link #id} to the specified {@link Long}.
     *
     * @param id
     *            the new {@link #id}
     */
    public void setId(Long id) {
        this.id = id;
    }


}
