package cz.kprochazka.af.rest.example.service;

import cz.kprochazka.af.rest.example.model.Country;

/**
 * {@link Country} storage.
 *
 * @author Kamil Prochazka
 */
public interface CountryStorage extends Storage<Country> {
}
