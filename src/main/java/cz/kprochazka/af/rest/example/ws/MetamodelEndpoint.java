package cz.kprochazka.af.rest.example.ws;

import com.codingcrayons.aspectfaces.plugins.j2ee.rest.builder.AFEntityBuilder;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

import cz.kprochazka.af.rest.example.model.AbsenceInstance;
import cz.kprochazka.af.rest.example.model.AbsenceType;
import cz.kprochazka.af.rest.example.model.Address;
import cz.kprochazka.af.rest.example.model.Country;
import cz.kprochazka.af.rest.example.model.Person;

/**
 * Simply returns metamodel for given mapped class.
 *
 * @author Kamil Prochazka
 */
@RestController
public class MetamodelEndpoint extends BaseAFEndpoint {

    private static final Map<String, Class> MAPPED_ENTITIES = new HashMap<>();

    static {
        MAPPED_ENTITIES.put(Address.class.getSimpleName(), Address.class);
        MAPPED_ENTITIES.put(Country.class.getSimpleName(), Country.class);
        MAPPED_ENTITIES.put(Person.class.getSimpleName(), Person.class);
        MAPPED_ENTITIES.put(AbsenceType.class.getSimpleName(), AbsenceType.class);
        MAPPED_ENTITIES.put(AbsenceInstance.class.getSimpleName(), AbsenceInstance.class);
    }

    @RequestMapping(value = "/rest/metamodel/{className}")
    public HttpEntity generateMetamodel(@PathVariable("className") String className) {
        Class entityClass = MAPPED_ENTITIES.get(className);
        if (entityClass == null) {
            return ResponseEntity.notFound().build();
        }

        AFEntityBuilder entityBuilder = restApplication.getEntityBuilder();
        entityBuilder.of(entityClass);

        return ResponseEntity.ok(entityBuilder.build());
    }

}
