package cz.kprochazka.af.rest.example.model;

import com.codingcrayons.aspectfaces.annotations.UiLabel;
import com.codingcrayons.aspectfaces.annotations.UiType;
import com.codingcrayons.aspectfaces.plugins.j2ee.rest.annotations.UiOptionValue;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Parent class for absence entities with LONG ids.
 *
 * @author Kamil Prochazka
 */
@MappedSuperclass
public abstract class EntityObject extends IdGeneratedEntityObject implements Serializable {

    @Temporal(TemporalType.TIMESTAMP)
    protected Date created;

    @Temporal(TemporalType.TIMESTAMP)
    protected Date modified;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @UiType(value = "id")
    @UiLabel("Entity.Id")
    @UiOptionValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    @PreUpdate
    protected void preUpdate() {
        this.modified = new Date();
    }

    @PrePersist
    protected void prePersist() {
        if (this.created == null) {
            this.created = new Date();
        }
    }

}
