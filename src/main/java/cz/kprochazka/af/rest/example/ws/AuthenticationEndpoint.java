package cz.kprochazka.af.rest.example.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import cz.kprochazka.af.rest.example.model.Person;
import cz.kprochazka.af.rest.example.model.UserRole;
import cz.kprochazka.af.rest.example.security.SecurityHelper;
import cz.kprochazka.af.rest.example.service.PersonStorage;

/**
 * Authentication endpoint providing user roles to clients.
 * Actual authentication is done by Spring Security.
 *
 * @author Kamil Prochazka
 */
@RestController
public class AuthenticationEndpoint {

    @Autowired
    private PersonStorage personStorage;

    @RequestMapping(value = "/rest/userDetails")
    public HttpEntity authenticateUserDetails() {
        User authenticatedUser = SecurityHelper.getAuthenticatedUser();
        if (authenticatedUser == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        Person person = personStorage.findByUsername(authenticatedUser.getUsername());
        if (person == null || !person.getActive()) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        UserDetail userDetail = new UserDetail();
        userDetail.setId(person.getId());
        userDetail.setUsername(person.getUsername());
        userDetail.setPassword(person.getPassword());
        userDetail.setEmail(person.getEmail());
        userDetail.setFirstName(person.getFirstName());
        userDetail.setLastName(person.getLastName());
        userDetail.setGender(person.getGender().name());

        List<String> roles = new ArrayList<>(2);
        for (UserRole role : person.getUserRoles()) {
            roles.add(role.name());
        }
        userDetail.setRoles(roles);

        return ResponseEntity.ok(userDetail);
    }

    public static class UserDetail {

        private Long id;

        private String username;
        private String password;
        private String email;

        private String firstName;
        private String lastName;

        private String gender;

        private List<String> roles;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public List<String> getRoles() {
            return roles;
        }

        public void setRoles(List<String> roles) {
            this.roles = roles;
        }
    }

}
