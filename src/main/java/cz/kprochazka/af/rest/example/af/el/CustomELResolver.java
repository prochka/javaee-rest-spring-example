package cz.kprochazka.af.rest.example.af.el;

import java.beans.FeatureDescriptor;
import java.util.Iterator;
import java.util.Map;

import javax.el.ELContext;
import javax.el.ELResolver;
import javax.el.PropertyNotFoundException;

/**
 * Custom application ELResolver which resolves against internal Map stored in ELContext under {@linkplain CustomELResolver#getClass()}.
 *
 * @author Kamil Prochazka
 */
public class CustomELResolver extends ELResolver {

    @Override
    public Object getValue(ELContext context, Object base, Object property) {
        if (base != null) {
            return null;
        }
        if (property == null) {
            throw new PropertyNotFoundException("Property not found :(");
        }

        Map<String, Object> map = retrieveMap(context);

        Object value = map.get((String) property);
        if (value != null) {
            context.setPropertyResolved(true);
            return value;
        }

        return null;
    }

    @Override
    public Class<?> getType(ELContext context, Object base, Object property) {
        if (base != null) {
            return null;
        }
        if (property == null) {
            throw new PropertyNotFoundException("Property not found :(");
        }

        return null;
    }

    @Override
    public void setValue(ELContext context, Object base, Object property, Object value) {
        if (base != null) {
            return;
        }
        if (property == null) {
            throw new PropertyNotFoundException("Property not found :(");
        }

        Map<String, Object> map = retrieveMap(context);
        String attribute = (String) property;
        if (map.containsKey(attribute)) {
            context.setPropertyResolved(true);
            map.put(attribute, value);
        }
    }

    @Override
    public boolean isReadOnly(ELContext context, Object base, Object property) {
        return false;
    }

    @Override
    public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext context, Object base) {
        return null;
    }

    @Override
    public Class<?> getCommonPropertyType(ELContext context, Object base) {
        if (base != null) {
            return null;
        }
        return Object.class;
    }

    private Map<String, Object> retrieveMap(ELContext elContext) {
        Map<String, Object> map = (Map<String, Object>) elContext.getContext(getClass());
        if (map == null) {
            throw new IllegalStateException("The internal Map was not present in ELContext! Is correctly configured CustomELContextListener in web.xml ?");
        }

        return map;
    }


}
